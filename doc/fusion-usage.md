# Fusion

[TOC]

## Variables d'environnement

| Variable                                     | Valeur par défaut | Description                                           |
| ---                                          | ---               | ---                                                   |
| `PHP_OFFICE_CLIENTS_FUSION_DEFAULT_STRATEGY` | `rest`            | La stratégie à utiliser par défaut (`rest` ou `soap`) |

### _rest_

@todo

| Variable                                                      | Valeur par défaut         |
| ---                                                           | ---                       |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_BASE_URI`             | `http://flow:8080`        |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PATH_FUSION`          | `/ODFgedooo/rest/fusion`  |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PATH_VERSION`         | `/ODFgedooo/rest/version` |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY`                | `0`                       |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY_HOST`           |                           |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY_PORT`           |                           |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY_USERNAME`       |                           |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY_PASSWORD`       |                           |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_VERBOSE`              | `0`                       |
| `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_EXPECTED_HTTP_STATUS` | `200`                     |

### _soap_

@todo

__ATTENTION:__ le service _flow_ ne contient plus _soap_

| Variable                                                      | Valeur par défaut  |
| ---                                                           | ---                |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_BASE_URI`             | `http://flow:8080` |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PATH_FUSION`          | `Fusion`           |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PATH_VERSION`         | `Version`          |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY`                | `0`                |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_HOST`           |                    |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_PORT`           |                    |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_USERNAME`       |                    |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_PASSWORD`       |                    |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_VERBOSE`              | `0`                |
| `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_EXPECTED_HTTP_STATUS` | `200`              |

---

## ...

Création du client avec le paramétrage par défaut.

```php
use Libriciel\OfficeClients\Fusion\Client\ClientFactory;
$client = ClientFactory::create();
```

| Constante | Valeur |
| --- | --- |
| `ClientFactory::REST` | `rest` |
| `ClientFactory::SOAP` | `soap` |

| Paramètre | Variable d'environnement | Valeur par défaut |
| --- | --- | --- |
| `$method` | `PHP_OFFICE_CLIENTS_FUSION_DEFAULT_STRATEGY` | `rest`  |

@todo: inverser "Valeur par défaut" et "Variable d'environnement" ci-dessous

REST

| Paramètre | Valeur par défaut | Variable d'environnement |
| --- | --- | --- |
| `$url` | `http://flow:8080` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_URL` |
| `$fusion` | `/ODFgedooo/rest/fusion` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_FUSION` |
| `$version` | `/ODFgedooo/rest/version` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_VERSION` |
| `$verbose` | `0` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_VERBOSE` |
| `$proxy` | `null` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY` |
| `$expectedHttpStatus` | `200` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_EXPECTED_HTTP_STATUS` |

| Paramètre | Valeur par défaut | Variable d'environnement |
| --- | --- | --- |
| `$proxy->host` | `null` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY_HOST` |
| `$proxy->port` | `null` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY_PORT` |
| `$proxy->username` | `null` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY_USERNAME` |
| `$proxy->password` | `null` | `PHP_OFFICE_CLIENTS_FUSION_REST_DEFAULT_PROXY_PASSWORD` |


SOAP

| Paramètre | Valeur par défaut | Variable d'environnement |
| --- | --- | --- |
| `$url` | `http://flow:8080` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_URL` |
| `$fusion` | `Fusion` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_FUSION` |
| `$version` | `Version` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_VERSION` |
| `$verbose` | `0` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_VERBOSE` |
| `$proxy` | `null` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY` |
| `$expectedHttpStatus` | `200` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_EXPECTED_HTTP_STATUS` |

| Paramètre | Valeur par défaut | Variable d'environnement |
| --- | --- | --- |
| `$proxy->host` | `null` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_HOST` |
| `$proxy->port` | `null` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_PORT` |
| `$proxy->username` | `null` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_USERNAME` |
| `$proxy->password` | `null` | `PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_PASSWORD` |

Récupération de la version du service distant correspondant. Par exemple, `2.1.8-546981` pour le service _REST_, `1.0.1`
pour le service _SOAP_.

```php
$version = $client->version();
```

Préparation des données pour la génération d'un document.

```php
use Libriciel\OfficeClients\Fusion\Helper\Builder;

// Données de la section "Projets"
$projets = [];
for ($idx = 1; $idx <= 3; $idx++) {
    $builder = (new Builder())
        ->addField('project_text_field', 'Projet ' . $idx, Builder::TYPE_STRING);
    $projets[] = $builder->getPart();
}

// Partie principale
$builder = (new Builder())
    ->addField('date_field', '1979-01-24', Builder::TYPE_DATE)
    ->addField('lines_field', "bar\nbaz", Builder::TYPE_LINES)
    ->addField('number_field_integer', 30, Builder::TYPE_NUMBER)
    ->addField('number_field_float', 30.65, Builder::TYPE_NUMBER)
    // @todo: addOdtContentFromFile(string $target, string $name, string $path, string $mime = null)
    // @fixme: vérifier que ce soit toujours de l'ODT
    ->addOdtContent('odt_content', 'part.odt', file_get_contents('/data/workspace/part.odt'))
    ->addField('string_field', 'boz', Builder::TYPE_STRING)
    ->addField('text_field', 'buz', Builder::TYPE_TEXT)
    ->addIteration('Projets', $projets)
;
```

| Constante | Valeur |
| --- | --- |
| `Builder::TYPE_DATE` | `date` |
| `Builder::TYPE_LINES` | `lines` |
| `Builder::TYPE_NUMBER` | `number` |
| `Builder::TYPE_STRING` | `string` |
| `Builder::TYPE_TEXT` | `text` |

Génération du document avec un gabarit et les données préparées ci-dessus.

```php
$documentContent = $client->fusion('/data/workspace/template.odt', $builder->getPart());
```

---

## @todo

```php
// Création d'un client REST
use Libriciel\OfficeClients\Fusion\Client\Configuration\RestServiceConfiguration;

// Équivalent à ClientFactory::create(ClientFactory::REST, new UnoserverServiceConfiguration('http://flow:8080'))
$client = ClientFactory::create();

$client = ClientFactory::create(ClientFactory::REST, new RestServiceConfiguration('http://gedooo:8080'));

// Création d'un client SOAP (supprimé dans flow)
use Libriciel\OfficeClients\Fusion\Client\Configuration\SoapServiceConfiguration;

$client = ClientFactory::create(ClientFactory::SOAP, new SoapServiceConfiguration('http://gedooo:8080'));
```

```php
// Récupération de la version du service distant correspondant
$version = $client->version();
```
### Ajouts

#### _Builder_

Mélange entre les patrons de conception _facade_ et _builder_, permet de dialoguer avec une seule classe plutôt qu'avec
les classes (`Libriciel\OfficeClients\Fusion\Type`) `ContentType`, `FieldType`, `IterationType` et `PartType` au moyen d'une
[_fluent interface_](https://en.wikipedia.org/wiki/Fluent_interface).

```php
use Libriciel\OfficeClients\Fusion\Helper\Builder;

// Préparation des données pour la génération d'un document
$projets = [];
for ($idx = 1; $idx <= 3; $idx++) {
    $builder = (new Builder())
        ->addField('project_text_field', 'Projet ' . $idx, Builder::TYPE_STRING);
    $projets[] = $builder->getPart();
}

$builder = (new Builder())
    ->addField('date_field', '1979-01-24', Builder::TYPE_DATE)
    ->addField('lines_field', "bar\nbaz", Builder::TYPE_LINES)
    ->addField('number_field_integer', 30, Builder::TYPE_NUMBER)
    ->addField('number_field_float', 30.65, Builder::TYPE_NUMBER)
    ->addOdtContent('odt_content', 'part.odt', file_get_contents('/data/workspace/part.odt'))
    ->addField('string_field', 'boz', Builder::TYPE_STRING)
    ->addField('text_field', 'buz', Builder::TYPE_TEXT)
    ->addIteration('Projets', $projets)
;
$main = $builder->getPart();
```

```php
use Libriciel\OfficeClients\Fusion\Helper\Builder;

// Préparation des données pour la génération d'un document
$projets = [];
for ($idx = 1; $idx <= 3; $idx++) {
    $builder = (new Builder())
        ->addStringField('project_text_field', 'Projet ' . $idx);
    $projets[] = $builder->getPart();
}

$builder = (new Builder())
    ->addDateField('date_field', '1979-01-24')
    ->addLinesField('lines_field', "bar\nbaz")
    ->addNumberField('number_field_integer', 30)
    ->addNumberField('number_field_float', 30.65)
    ->addOdtContent('odt_content', 'part.odt', file_get_contents('/data/workspace/part.odt'))
    ->addContent(
        'odt_content',
        'part.odt',
        file_get_contents('/data/workspace/part.odt'),
        Builder::MIME_ODT
    )
    ->addStringField('string_field', 'boz')
    ->addTextField('text_field', 'buz')
    ->addIteration('Projets', $projets)
;
$main = $builder->getPart();
```

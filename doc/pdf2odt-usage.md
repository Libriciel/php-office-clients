# Pdf2Odt

[TOC]

## Variables d'environnement

| Variable                                      | Valeur par défaut | Description                                                        |
| ---                                           | ---               |--------------------------------------------------------------------|
| `PHP_OFFICE_CLIENTS_PDF2ODT_DEFAULT_STRATEGY` | `lspdf2odt`       | La stratégie à utiliser par défaut (`lspdf2odt` ou `vectorstodoc`) |

### _lspdf2odt_

| Variable                                                            | Valeur par défaut                 |
| ---                                                                 | ---                               |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_BASE_URI`             | `http://lspdf2odt`                |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PATH`                 | `/api/v1/pdf2odt`                 |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_METHOD`               | `ghostscript`                     |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_RESOLUTION`           | `150`                             |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY`                | `0`                               |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY_HOST`           |                                   |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY_PORT`           |                                   |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY_USERNAME`       |                                   |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY_PASSWORD`       |                                   |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_VERBOSE`              | `0`                               |
| `PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_EXPECTED_HTTP_STATUS` | `201`                             |

### _vectorstodoc_

| Variable                                                               | Valeur par défaut          |
|------------------------------------------------------------------------|----------------------------|
| `PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_BASE_URI`             | `http://vectorstodoc:8080` |
| `PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PATH`                 | `/api/v1/pdf-to-odt`       |
| `PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY`                | `0`                        |
| `PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY_HOST`           |                            |
| `PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY_PORT`           |                            |
| `PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY_USERNAME`       |                            |
| `PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY_PASSWORD`       |                            |
| `PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_VERBOSE`              | `0`                        |
| `PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_EXPECTED_HTTP_STATUS` | `200`                      |

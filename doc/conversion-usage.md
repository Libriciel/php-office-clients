# Conversion

[TOC]

## Variables d'environnement

| Variable                                         | Valeur par défaut | Description                                                    |
| ---                                              | ---               | ---                                                            |
| `PHP_OFFICE_CLIENTS_CONVERSION_DEFAULT_STRATEGY` | `unoserver`       | La stratégie à utiliser par défaut (`cloudooo` ou `unoserver`) |

### _cloudooo_
@fixme
| Variable                                                              | Valeur par défaut      |
| ---                                                                   | ---                    |
| `PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_BASE_URI`             | `http://cloudooo:8011` |
| `PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PATH`                 | `convertFile`          |
| `PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY`                | `0`                    |
| `PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY_HOST`           |                        |
| `PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY_PORT`           |                        |
| `PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY_USERNAME`       |                        |
| `PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY_PASSWORD`       |                        |
| `PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_VERBOSE`              | `0`                    |
| `PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_EXPECTED_HTTP_STATUS` | `200`                  |

### _unoserver_
@fixme
| Variable                                                               | Valeur par défaut       |
| ---                                                                    | ---                     |
| `PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER_DEFAULT_BASE_URI`             | `http://unoserver:2004` |
| `PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER_DEFAULT_PATH`                 | `/request`              |
| `PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER_DEFAULT_PROXY`                | `0`                     |
| `PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER_DEFAULT_PROXY_HOST`           |                         |
| `PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER_DEFAULT_PROXY_PORT`           |                         |
| `PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER_DEFAULT_PROXY_USERNAME`       |                         |
| `PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER_DEFAULT_PROXY_PASSWORD`       |                         |
| `PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER_DEFAULT_VERBOSE`              | `0`                     |
| `PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER_DEFAULT_EXPECTED_HTTP_STATUS` | `200`                   |

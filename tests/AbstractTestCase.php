<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Fusion\Type\ContentType;
use Libriciel\OfficeClients\Fusion\Type\FieldType;
use Libriciel\OfficeClients\Fusion\Type\IterationType;
use Libriciel\OfficeClients\Fusion\Type\PartType;
use PHPUnit\Framework\TestCase;
use ZipArchive;

abstract class AbstractTestCase extends TestCase
{
    protected function getBasePath(): string
    {
        return dirname(__DIR__);
    }

    protected function getFixturePath(string $relative): string
    {
        return $this->getBasePath()
            . DIRECTORY_SEPARATOR . 'tests'
            . DIRECTORY_SEPARATOR . 'Fixture'
            . DIRECTORY_SEPARATOR . $relative;
    }

    protected function getVarPath(string $relative): string
    {
        return $this->getBasePath()
            . DIRECTORY_SEPARATOR . 'var'
            . DIRECTORY_SEPARATOR . $relative;
    }

    protected function getCommonPartType()
    {
        $main = new PartType();

        $main->addElement(new FieldType('date_field', '1979-01-24', 'date'));
        $main->addElement(new FieldType('lines_field', "bar\nbaz", 'lines'));
        $main->addElement(new FieldType('number_field_integer', 30, 'number'));
        $main->addElement(new FieldType('number_field_float', 30.65, 'number'));
        $main->addElement(new ContentType(
            'odt_content',
            'part.odt',
            'application/vnd.oasis.opendocument.text',
            'binary',
            file_get_contents($this->getFixturePath('part.odt'))
        ));
        $main->addElement(new FieldType('string_field', 'boz', 'string'));
        $main->addElement(new FieldType('text_field', 'buz', 'text'));

        // Ajout d'une section "Projets" contenant 3 projets
        $section = new IterationType('Projets');

        for ($idx = 1; $idx <= 3; $idx++) {
            $part = new PartType();
            $part->addElement(new FieldType('project_text_field', 'Projet ' . $idx, 'string'));
            $section->addPart($part);
        }

        $main->addElement($section);

        return $main;
    }

    protected function saveTestResult(string $method, string $content, string $ext)
    {
        $basename = preg_replace('/^.*\\\\([^\\\\]+)::(.*)$/', '\1-\2.' . $ext, $method);
        $path = $this->getVarPath('tests' . DIRECTORY_SEPARATOR . $basename);
        file_put_contents($path, $content);
        return 'var' . DIRECTORY_SEPARATOR . 'tests' . DIRECTORY_SEPARATOR . $basename;
    }

    protected function saveOdtGetPicturesList(string $method, string $odtContent)
    {
        $relative = $this->saveTestResult($method, $odtContent, 'odt');
        $path = $this->getBasePath() . DIRECTORY_SEPARATOR . $relative;
        $odt = new ZipArchive();
        $ret = $odt->open($path, ZipArchive::RDONLY);
        if ($ret !== true) {
            $message = sprintf('Could not open ZIP archive "%s" (error code %d)', $path, $ret);
            throw new RuntimeException($message);
        }
        $pictures = [];
        for ($idx = 0; $idx < $odt->numFiles; $idx++) {
            $entry = $odt->statIndex($idx);
            if (str_starts_with($entry['name'], 'IMAGE_DIRECTORY') || str_starts_with($entry['name'], 'Pictures/')) {
                $pictures[] = preg_replace('/^Pictures\/(.*)$/', '\1', $entry['name']);
            }
        }
        $odt->close();

        return $pictures;
    }

    protected function saveOdtGetContentXml(string $method, string $odtContent)
    {
        $relative = $this->saveTestResult($method, $odtContent, 'odt');
        $path = $this->getBasePath() . DIRECTORY_SEPARATOR . $relative;
        $odt = new ZipArchive();
        $ret = $odt->open($path, ZipArchive::RDONLY);
        if ($ret !== true) {
            $message = sprintf('Could not open ZIP archive "%s" (error code %d)', $path, $ret);
            throw new RuntimeException($message);
        }
        $content = $odt->getFromName('content.xml');
        $odt->close();

        return $content;
    }

    protected function getExpectedFlowContentXml()
    {
        return file_get_contents($this->getFixturePath('flow-expected-content.xml'));
    }

    protected function getExpectedGedoooContentXml()
    {
        return file_get_contents($this->getFixturePath('gedooo-expected-content.xml'));
    }

    protected function getObjectAttributes(object $object): array
    {
        return json_decode(json_encode((array)$object), true);
    }

    protected function exportConfig(object $object)
    {
        return array_values($this->getObjectAttributes($object))[0];
    }

    protected function getDefaultLspdf2odtConfiguration(): array
    {
        return [
            'baseUri' => 'http://lspdf2odt',
            'path' => '/api/v1/pdf2odt',
            'proxy' => null,
            'method' => 'ghostscript',
            'resolution' => 150,
            'verbose' => false,
            'expectedHttpStatus' => 201,
        ];
    }

    protected function getDefaultVectorstodocConfiguration(): array
    {
        return [
            'baseUri' => 'http://vectorstodoc:8080',
            'path' => '/api/v1/pdf-to-odt',
            'proxy' => null,
            'verbose' => false,
            'expectedHttpStatus' => 200,
        ];
    }

    protected function getDefaultRestConfiguration(): array
    {
        return [
            'baseUri' => 'http://flow:8080',
            'proxy' => null,
            'pathFusion' => '/ODFgedooo/rest/fusion',
            'pathVersion' => '/ODFgedooo/rest/version',
            'verbose' => false,
            'expectedHttpStatus' => 200,
        ];
    }

    protected function getDefaultSoapConfiguration(): array
    {
        return [
            'baseUri' => 'http://flow:8080',
            'proxy' => null,
            'pathFusion' => 'Fusion',
            'pathVersion' => 'Version',
            'verbose' => false,
            'expectedHttpStatus' => 200,
        ];
    }

    protected function getDefaultUnoserverConfiguration(): array
    {
        return [
            'baseUri' => 'http://unoserver:2004',
            'path' => '/request',
            'proxy' => null,
            'verbose' => false,
            'expectedHttpStatus' => 200,
            'useStream' => true,
        ];
    }

    protected function getDefaultCloudoooConfiguration(): array
    {
        return [
            'baseUri' => 'http://cloudooo:8011',
            'path' => 'convertFile',
            'proxy' => null,
            'verbose' => false,
            'expectedHttpStatus' => 200,
            'useCurl' => 2,
        ];
    }

    protected function getProxyConfiguration(
        string $host = null,
        int $port = null,
        string $username = null,
        string $password = null
    ): ServiceProxyConfiguration {
        return new ServiceProxyConfiguration(
            $host ?: 'proxy',
            $port ?: 3128,
            $username ?: null,
            $password ?: null
        );
    }

    protected function getProxyAuthConfiguration(
        string $host = null,
        int $port = null,
        string $username = null,
        string $password = null
    ): ServiceProxyConfiguration {
        return new ServiceProxyConfiguration(
            $host ?: 'proxy-auth',
            $port ?: 3128,
            $username ?: 'username',
            $password ?: 'password'
        );
    }

    public function dataProxyConfigurationSuccess()
    {
        return [
            [null],
//            [$this->getProxyConfiguration()],
//            [$this->getProxyAuthConfiguration()],
        ];
    }
}

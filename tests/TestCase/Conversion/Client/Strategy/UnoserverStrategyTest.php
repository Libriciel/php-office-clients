<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Conversion\Client\Strategy;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Conversion\Client\Configuration\UnoserverServiceConfiguration;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Conversion\Client\Strategy\UnoserverStrategy;
use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class UnoserverStrategyTest extends AbstractTestCase
{
    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testConversionSuccess(?ServiceProxyConfiguration $proxy)
    {
        $config = new UnoserverServiceConfiguration(null, null, $proxy);
        $strategy = new UnoserverStrategy($config);
        $pdfContent = $strategy->conversion(file_get_contents($this->getFixturePath('template.odt')));
        $this->assertNotEmpty($pdfContent);
        $this->saveTestResult(__METHOD__, $pdfContent, 'pdf');
    }

    public function testConversionFailureConnectionException()
    {
        $config = new UnoserverServiceConfiguration('http://unoserver:80');
        $strategy = new UnoserverStrategy($config);

        $this->expectException(ConnectionException::class);
        $message = 'Could not reach http://unoserver:80/request';
        $this->expectExceptionMessage($message);
        $strategy->conversion(file_get_contents($this->getFixturePath('template.odt')));
    }

    public function testConversionFailureUnexpectedHttpStatusException()
    {
        $config = new UnoserverServiceConfiguration(null, null, null, null, 201);
        $strategy = new UnoserverStrategy($config);

        $this->expectException(UnexpectedHttpStatusException::class);
        $message = "Got HTTP status 200 instead of expected HTTP status 201 calling http://unoserver:2004" ;
        $this->expectExceptionMessage($message);
        $strategy->conversion(file_get_contents($this->getFixturePath('template.odt')));
    }
}

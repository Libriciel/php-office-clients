<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Conversion\Client\Strategy;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Conversion\Client\Configuration\CloudoooServiceConfiguration;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Conversion\Client\Strategy\CloudoooStrategy;
use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class CloudoooStrategyTest extends AbstractTestCase
{
    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testConversionSuccess(?ServiceProxyConfiguration $proxy)
    {
        $config = new CloudoooServiceConfiguration(null, null, $proxy);
        $strategy = new CloudoooStrategy($config);
        $pdfContent = $strategy->conversion(file_get_contents($this->getFixturePath('template.odt')));
        $this->assertNotEmpty($pdfContent);
        $this->saveTestResult(__METHOD__, $pdfContent, 'pdf');
    }

    public function testConversionFailureConnectionException()
    {
        $config = new CloudoooServiceConfiguration('http://cloudooo:80');
        $strategy = new CloudoooStrategy($config);

        $this->expectException(ConnectionException::class);
        $message = 'Could not reach http://cloudooo:80';
        $this->expectExceptionMessage($message);
        $strategy->conversion(file_get_contents($this->getFixturePath('template.odt')));
    }

    public function testConversionFailureUnexpectedHttpStatusException()
    {
        $config = new CloudoooServiceConfiguration(null, null, null, null, 201);
        $strategy = new CloudoooStrategy($config);

        $this->expectException(UnexpectedHttpStatusException::class);
        $message = 'Got HTTP status 200 instead of expected HTTP status 201 calling http://cloudooo:8011';
        $this->expectExceptionMessage($message);
        $strategy->conversion(file_get_contents($this->getFixturePath('template.odt')));
    }
}

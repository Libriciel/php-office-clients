<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Conversion\Client;

use Libriciel\OfficeClients\Conversion\Client\ClientFactory;
use Libriciel\OfficeClients\Conversion\Client\Configuration\UnoserverServiceConfiguration;
use Libriciel\OfficeClients\Conversion\Client\Configuration\CloudoooServiceConfiguration;
use Libriciel\OfficeClients\Exception\InvalidStrategyException;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class ClientFactoryTest extends AbstractTestCase
{
    public function testCreateDefaultStrategySuccess()
    {
        $actual = ClientFactory::create();

        $this->assertInstanceOf('\Libriciel\OfficeClients\Conversion\Client\Strategy\UnoserverStrategy', $actual);
        $this->assertEquals($this->getDefaultUnoserverConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateOverloadedEnvVarStrategySuccess()
    {
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_DEFAULT_STRATEGY=cloudooo');
        $actual = ClientFactory::create();

        $this->assertInstanceOf('\Libriciel\OfficeClients\Conversion\Client\Strategy\CloudoooStrategy', $actual);
        $this->assertEquals($this->getDefaultCloudoooConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateCloudoooStrategyDefaultConfigurationSuccess()
    {
        $actual = ClientFactory::create('cloudooo');

        $this->assertInstanceOf('\Libriciel\OfficeClients\Conversion\Client\Strategy\CloudoooStrategy', $actual);
        $this->assertEquals($this->getDefaultCloudoooConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateUnoserverStrategyDefaultConfigurationSuccess()
    {
        $actual = ClientFactory::create('unoserver');

        $this->assertInstanceOf('\Libriciel\OfficeClients\Conversion\Client\Strategy\UnoserverStrategy', $actual);
        $this->assertEquals($this->getDefaultUnoserverConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateUnknownStrategyFailure()
    {
        $this->expectException(InvalidStrategyException::class);
        $this->expectExceptionMessage('Invalid strategy "unknown" requested, use one of "cloudooo", "unoserver".');

        ClientFactory::create('unknown');
    }

    public function testCreateCloudoooStrategyCustomConfigurationSuccess()
    {
        $actual = ClientFactory::create('cloudooo', new CloudoooServiceConfiguration('http://othercloudooo:1234'));

        $this->assertInstanceOf('\Libriciel\OfficeClients\Conversion\Client\Strategy\CloudoooStrategy', $actual);
        $expected = array_merge($this->getDefaultCloudoooConfiguration(), ['baseUri' => 'http://othercloudooo:1234']);
        $this->assertEquals($expected, $this->exportConfig($actual));
    }

    public function testCreateUnoserverStrategyCustomConfigurationSuccess()
    {
        $actual = ClientFactory::create('unoserver', new UnoserverServiceConfiguration('http://otherunoserver:1234'));

        $this->assertInstanceOf('\Libriciel\OfficeClients\Conversion\Client\Strategy\UnoserverStrategy', $actual);
        $expected = array_merge($this->getDefaultUnoserverConfiguration(), ['baseUri' => 'http://otherunoserver:1234']);
        $this->assertEquals($expected, $this->exportConfig($actual));
    }
}

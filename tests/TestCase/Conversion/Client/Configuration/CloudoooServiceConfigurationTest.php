<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Conversion\Client\Configuration;

use Libriciel\OfficeClients\Conversion\Client\Configuration\CloudoooServiceConfiguration;
use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class CloudoooServiceConfigurationTest extends AbstractTestCase
{
    public function testConstructDefaultEnvVars()
    {
        $actual = $this->getObjectAttributes(new CloudoooServiceConfiguration());
        $this->assertEquals($this->getDefaultCloudoooConfiguration(), $actual);
    }

    public function testConstructDefaultEnvVarsServiceProxyConfiguration()
    {
        $config = new CloudoooServiceConfiguration(
            null,
            null,
            new ServiceProxyConfiguration(
                'host_object',
                0,
                'username_object',
                'password_object'
            )
        );
        $actual = $this->getObjectAttributes($config);
        $expected = array_merge(
            $this->getDefaultCloudoooConfiguration(),
            [
                'proxy' => [
                    'host' => 'host_object',
                    'port' => 0,
                    'username' => 'username_object',
                    'password' => 'password_object',
                ],
            ]
        );
        $this->assertEquals($expected, $actual);
    }

    public function testConstructOverloadedConstructor()
    {
        $actual = $this->getObjectAttributes(
            new CloudoooServiceConfiguration(
                'baseUri_constructor',
                'path_constructor',
                new ServiceProxyConfiguration(
                    'host_constructor',
                    123,
                    'username_constructor',
                    'password_constructor'
                ),
                true,
                201
            )
        );
        $expected = [
            'baseUri' => 'baseUri_constructor',
            'path' => 'path_constructor',
            'proxy' => [
                'host' => 'host_constructor',
                'port' => 123,
                'username' => 'username_constructor',
                'password' => 'password_constructor',
            ],
            'verbose' => true,
            'expectedHttpStatus' => 201,
            'useCurl' => 2,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testConstructOverloadedEnvVars()
    {
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_BASE_URI=url_env');
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PATH=path_env');
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY=1');
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY_HOST=host_env');
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY_PORT=456');
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY_USERNAME=username_env');
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_PROXY_PASSWORD=password_env');
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_VERBOSE=0');
        putenv('PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO_DEFAULT_EXPECTED_HTTP_STATUS=202');

        $actual = $this->getObjectAttributes(new CloudoooServiceConfiguration());
        $expected = [
            'baseUri' => 'url_env',
            'path' => 'path_env',
            'proxy' => [
                'host' => 'host_env',
                'port' => 456,
                'username' => 'username_env',
                'password' => 'password_env',
            ],
            'verbose' => false,
            'expectedHttpStatus' => 202,
            'useCurl' => 2,
        ];
        $this->assertEquals($expected, $actual);
    }
}

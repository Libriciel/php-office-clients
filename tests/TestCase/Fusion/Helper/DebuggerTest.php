<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Fusion\Helper;

use Libriciel\OfficeClients\Fusion\Helper\Debugger;
use Libriciel\OfficeClients\Test\AbstractTestCase;

// @todo: tester avec plus de profondeur d'itérations
class DebuggerTest extends AbstractTestCase
{
    public function testAllPathsToCsvExportValuesFalse()
    {
        $actual = Debugger::allPathsToCsv($this->getCommonPartType());
        $expected = '"Chemins","Types"
"date_field","date"
"lines_field","lines"
"number_field_integer","number"
"number_field_float","number"
"string_field","string"
"text_field","text"
"Projets.0.project_text_field","string"
"Projets.1.project_text_field","string"
"Projets.2.project_text_field","string"
"part.odt","application/vnd.oasis.opendocument.text","odt_content"';
        $this->assertEquals($expected, $actual);
    }

    public function testAllPathsToCsvExportValuesTrue()
    {
        $actual = Debugger::allPathsToCsv($this->getCommonPartType(), true);
        $expected = '"Chemins","Types","Valeurs"
"date_field","date","1979-01-24"
"lines_field","lines","bar
baz"
"number_field_integer","number","30"
"number_field_float","number","30.65"
"string_field","string","boz"
"text_field","text","buz"
"Projets.0.project_text_field","string","Projet 1"
"Projets.1.project_text_field","string","Projet 2"
"Projets.2.project_text_field","string","Projet 3"
"part.odt","application/vnd.oasis.opendocument.text","odt_content","7404303579f08d1bcd3c367e8b7e5451"';
        $this->assertEquals($expected, $actual);
    }

    public function testHashPathsToCsv()
    {
        $actual = Debugger::hashPathsToCsv($this->getCommonPartType());
        $expected = '"Chemins","Types"
"date_field","date"
"lines_field","lines"
"number_field_integer","number"
"number_field_float","number"
"string_field","string"
"text_field","text"
"Projets.{n}.project_text_field","string"
"part.odt","application/vnd.oasis.opendocument.text"';
        $this->assertEquals($expected, $actual);
    }
}

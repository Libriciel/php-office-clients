<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Fusion\Helper;

use Libriciel\OfficeClients\Fusion\Helper\Builder;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class BuilderTest extends AbstractTestCase
{
    public function testAllSpecificMethods()
    {
        $projets = [];
        for ($idx = 1; $idx <= 3; $idx++) {
            $builder = (new Builder())
                ->addStringField('project_text_field', 'Projet ' . $idx);
            $projets[] = $builder->getPart();
        }

        $builder = (new Builder())
            ->addDateField('date_field', '1979-01-24')
            ->addLinesField('lines_field', "bar\nbaz")
            ->addNumberField('number_field_integer', 30)
            ->addNumberField('number_field_float', 30.65)
            ->addOdtContent('odt_content', 'part.odt', file_get_contents($this->getFixturePath('part.odt')))
            ->addStringField('string_field', 'boz')
            ->addTextField('text_field', 'buz')
            ->addIteration('Projets', $projets)
        ;
        $actual = $builder->getPart();

        $expected = $this->getCommonPartType();
        $this->assertEquals($expected, $actual);
    }

    public function testGenericMethods()
    {
        $projets = [];
        for ($idx = 1; $idx <= 3; $idx++) {
            $builder = (new Builder())
                ->addField('project_text_field', 'Projet ' . $idx, Builder::TYPE_STRING);
            $projets[] = $builder->getPart();
        }

        $builder = (new Builder())
            ->addField('date_field', '1979-01-24', Builder::TYPE_DATE)
            ->addField('lines_field', "bar\nbaz", Builder::TYPE_LINES)
            ->addField('number_field_integer', 30, Builder::TYPE_NUMBER)
            ->addField('number_field_float', 30.65, Builder::TYPE_NUMBER)
            ->addContent(
                'odt_content',
                'part.odt',
                file_get_contents($this->getFixturePath('part.odt')),
                Builder::MIME_ODT
            )
            ->addField('string_field', 'boz', Builder::TYPE_STRING)
            ->addField('text_field', 'buz', Builder::TYPE_TEXT)
            ->addIteration('Projets', $projets)
        ;
        $actual = $builder->getPart();

        $expected = $this->getCommonPartType();
        $this->assertEquals($expected, $actual);
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Fusion\Helper;

use Libriciel\OfficeClients\Fusion\Helper\Director;
use Libriciel\OfficeClients\Test\AbstractTestCase;
use Libriciel\OfficeClients\Fusion\Type\ContentType;
use Libriciel\OfficeClients\Fusion\Type\FieldType;
use Libriciel\OfficeClients\Fusion\Type\IterationType;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * @deprecated
 */
class DirectorTest extends AbstractTestCase
{
    public function testMain()
    {
        $odtDocument = file_get_contents($this->getFixturePath('part.odt'));
        $data = [
            'User' => [
                'id' => 4,
                'username' => 'cbuffin',
                'birthday' => '1979-01-24',
                'document' => $odtDocument,
            ]
        ];
        $types = [
            'user_id' => 'number',
            'user_username' => 'text',
            'user_birthday' => 'date',
            'user_document' => 'file',
        ];
        $correspondances = [
            'user_id' => 'User.id',
            'user_username' => 'User.username',
            'user_birthday' => 'User.birthday',
            'user_document' => 'User.document',
        ];

        $main = new PartType();
        $actual = Director::main($main, $data, $types, $correspondances);

        $expectedFields = [
            new FieldType('user_id', '4', 'number'),
            new FieldType('user_username', 'cbuffin', 'text'),
            new FieldType('user_birthday', '1979-01-24', 'date'),
        ];
        $this->assertEquals($expectedFields, $actual->field);

        $expectedContent = [
            new ContentType(
                'User.document',
                'User.document.odt',
                'application/vnd.oasis.opendocument.text',
                'binary',
                $odtDocument
            ),
        ];
        $this->assertEquals($expectedContent, $actual->content);
    }

    public function testIteration()
    {
        $data = [
            [
                'User' => [
                    'id' => 4,
                    'username' => 'cbuffin',
                    'birthday' => '1979-01-24',
                ],
            ],
            [
                'User' => [
                    'id' => 5,
                    'username' => 'pmason',
                    'birthday' => '1982-03-12',
                ],
            ],
        ];
        $types = [
            'Users.id' => 'number',
            'Users.username' => 'text',
            'Users.birthday' => 'date',
        ];
        $correspondances = [
            'Users.id' => 'user_id',
            'Users.username' => 'user_username',
            'Users.birthday' => 'user_birthday',
        ];

        $main = new PartType();
        $iterationName = 'Users';
        $iterationData = [
            $iterationName => [
                $data[0]['User'],
                $data[1]['User'],
            ]
        ];
        $actual = Director::iteration($main, $iterationName, $iterationData, $types, $correspondances);

        $expected = new IterationType($iterationName);
        $expected->addPart(Director::main(new PartType(), $data[0], $types, $correspondances));
        $expected->addPart(Director::main(new PartType(), $data[1], $types, $correspondances));
        $expected = [$expected];

        $this->assertEquals($expected, $actual->iteration);
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Client\Strategy;

use Libriciel\OfficeClients\Fusion\Client\Configuration\SoapServiceConfiguration;
use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class SoapServiceConfigurationTest extends AbstractTestCase
{
    public function testConstructDefaultEnvVars()
    {
        $actual = $this->getObjectAttributes(new SoapServiceConfiguration());
        $this->assertEquals($this->getDefaultSoapConfiguration(), $actual);
    }

    public function testConstructDefaultEnvVarsServiceProxyConfiguration()
    {
        $config = new SoapServiceConfiguration(
            null,
            null,
            null,
            new ServiceProxyConfiguration(
                'host_object',
                0,
                'username_object',
                'password_object'
            )
        );
        $actual = $this->getObjectAttributes($config);
        $expected = array_merge(
            $this->getDefaultSoapConfiguration(),
            [
                'proxy' => [
                    'host' => 'host_object',
                    'port' => 0,
                    'username' => 'username_object',
                    'password' => 'password_object',
                ],
            ]
        );
        $this->assertEquals($expected, $actual);
    }

    public function testConstructOverloadedConstructor()
    {
        $actual = $this->getObjectAttributes(
            new SoapServiceConfiguration(
                'baseUri_constructor',
                'pathFusion_constructor',
                'pathVersion_constructor',
                new ServiceProxyConfiguration(
                    'host_constructor',
                    123,
                    'username_constructor',
                    'password_constructor'
                ),
                true,
                201
            )
        );
        $expected = [
            'baseUri' => 'baseUri_constructor',
            'pathFusion' => 'pathFusion_constructor',
            'pathVersion' => 'pathVersion_constructor',
            'proxy' => [
                'host' => 'host_constructor',
                'port' => 123,
                'username' => 'username_constructor',
                'password' => 'password_constructor',
            ],
            'verbose' => true,
            'expectedHttpStatus' => 201,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testConstructOverloadedEnvVars()
    {
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_BASE_URI=baseUri_env');
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PATH_FUSION=pathFusion_env');
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PATH_VERSION=pathVersion_env');
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY=1');
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_HOST=host_env');
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_PORT=456');
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_USERNAME=username_env');
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_PROXY_PASSWORD=password_env');
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_VERBOSE=0');
        putenv('PHP_OFFICE_CLIENTS_FUSION_SOAP_DEFAULT_EXPECTED_HTTP_STATUS=202');

        $actual = $this->getObjectAttributes(new SoapServiceConfiguration());
        $expected = [
            'baseUri' => 'baseUri_env',
            'pathFusion' => 'pathFusion_env',
            'pathVersion' => 'pathVersion_env',
            'proxy' => [
                'host' => 'host_env',
                'port' => 456,
                'username' => 'username_env',
                'password' => 'password_env',
            ],
            'verbose' => false,
            'expectedHttpStatus' => 202,
        ];
        $this->assertEquals($expected, $actual);
    }
}

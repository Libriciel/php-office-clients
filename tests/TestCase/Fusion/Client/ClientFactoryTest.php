<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Fusion\Client;

use Libriciel\OfficeClients\Fusion\Client\ClientFactory;
use Libriciel\OfficeClients\Fusion\Client\Configuration\RestServiceConfiguration;
use Libriciel\OfficeClients\Fusion\Client\Configuration\SoapServiceConfiguration;
use Libriciel\OfficeClients\Exception\InvalidStrategyException;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class ClientFactoryTest extends AbstractTestCase
{
    public function testCreateDefaultStrategySuccess()
    {
        $actual = ClientFactory::create();

        $this->assertInstanceOf('\Libriciel\OfficeClients\Fusion\Client\Strategy\RestStrategy', $actual);
        $this->assertEquals($this->getDefaultRestConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateOverloadedEnvVarStrategySuccess()
    {
        putenv('PHP_OFFICE_CLIENTS_FUSION_DEFAULT_STRATEGY=soap');
        $actual = ClientFactory::create();

        $this->assertInstanceOf('\Libriciel\OfficeClients\Fusion\Client\Strategy\SoapStrategy', $actual);
        $this->assertEquals($this->getDefaultSoapConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateRestStrategyDefaultConfigurationSuccess()
    {
        $actual = ClientFactory::create('rest', new RestServiceConfiguration());

        $this->assertInstanceOf('\Libriciel\OfficeClients\Fusion\Client\Strategy\RestStrategy', $actual);
        $this->assertEquals($this->getDefaultRestConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateSoapStrategyDefaultConfigurationSuccess()
    {
        $actual = ClientFactory::create('soap', new SoapServiceConfiguration());

        $this->assertInstanceOf('\Libriciel\OfficeClients\Fusion\Client\Strategy\SoapStrategy', $actual);
        $this->assertEquals($this->getDefaultSoapConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateUnknownFailure()
    {
        $this->expectException(InvalidStrategyException::class);
        $this->expectExceptionMessage('Invalid strategy "unknown" requested, use one of "rest", "soap".');

        ClientFactory::create('unknown');
    }

    public function testCreateRestStrategyCustomConfigurationSuccess()
    {
        $actual = ClientFactory::create('rest', new RestServiceConfiguration('http://other:1234'));

        $this->assertInstanceOf('\Libriciel\OfficeClients\Fusion\Client\Strategy\RestStrategy', $actual);
        $expected = array_merge($this->getDefaultRestConfiguration(), ['baseUri' => 'http://other:1234']);
        $this->assertEquals($expected, $this->exportConfig($actual));
    }

    public function testCreateSoapStrategyCustomConfigurationSuccess()
    {
        $actual = ClientFactory::create('soap', new SoapServiceConfiguration('http://other:1234'));

        $this->assertInstanceOf('\Libriciel\OfficeClients\Fusion\Client\Strategy\SoapStrategy', $actual);
        $expected = array_merge($this->getDefaultSoapConfiguration(), ['baseUri' => 'http://other:1234']);
        $this->assertEquals($expected, $this->exportConfig($actual));
    }
}

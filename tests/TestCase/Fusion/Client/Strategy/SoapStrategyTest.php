<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Fusion\Client\Strategy;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use Libriciel\OfficeClients\Fusion\Client\Configuration\SoapServiceConfiguration;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Fusion\Client\Strategy\SoapStrategy;
use Libriciel\OfficeClients\Fusion\Exception\InvalidTemplateException;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class SoapStrategyTest extends AbstractTestCase
{
    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testFlowFusionSuccess(?ServiceProxyConfiguration $proxy)
    {
        $this->markTestSkipped('flow ne contient plus le service SOAP, vérifier que l\'on obtienne bien une 410');

        $config = new SoapServiceConfiguration('http://flow:8080/ODFgedooo/OfficeService?wsdl', null, null, $proxy);
        $strategy = new SoapStrategy($config);
        $odtContent = $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
        $actual = $this->saveOdtGetContentXml(__METHOD__, $odtContent);
        $this->assertXmlStringEqualsXmlString($this->getExpectedFlowContentXml(), $actual);
    }

    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testFlowVersionSuccess(?ServiceProxyConfiguration $proxy)
    {
        $this->markTestSkipped('flow ne contient plus le service SOAP, vérifier que l\'on obtienne bien une 410');

        $config = new SoapServiceConfiguration('http://flow:8080/ODFgedooo/OfficeService?wsdl', null, null, $proxy);
        $strategy = new SoapStrategy($config);
        $actual = $strategy->version();
        $this->assertEquals('1.0.1', $actual);
    }

    public function testFlowVersionFailure()
    {
        $this->markTestSkipped('flow ne contient plus le service SOAP, vérifier que l\'on obtienne bien une 410');

        $this->expectException(ConnectionException::class);
        $message = 'SOAP-ERROR: Parsing WSDL: Couldn\'t load from \'http://flow:80/ODFgedooo/OfficeService?wsdlx\''
            . ' : failed to load external entity "http://flow:80/ODFgedooo/OfficeService?wsdlx"';
        $this->expectExceptionMessage($message);

        $config = new SoapServiceConfiguration('http://flow:80/ODFgedooo/OfficeService?wsdlx');
        $strategy = new SoapStrategy($config);
        $strategy->version();
    }

    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testGedoooFusionSuccess(?ServiceProxyConfiguration $proxy)
    {
        $config = new SoapServiceConfiguration('http://gedooo:8080/ODFgedooo/OfficeService?wsdl', null, null, $proxy);
        $strategy = new SoapStrategy($config);
        $odtContent = $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
        $actual = $this->saveOdtGetContentXml(__METHOD__, $odtContent);
        $this->assertXmlStringEqualsXmlString($this->getExpectedGedoooContentXml(), $actual);
    }

    public function testGedoooFusionFailureConnectionException()
    {
        $this->expectException(ConnectionException::class);
        $message = 'SOAP-ERROR: Parsing WSDL: Couldn\'t load from \'http://gedooo:8080/ODFgedooo/OfficeService?wsdlx\''
            . ' : failed to load external entity "http://gedooo:8080/ODFgedooo/OfficeService?wsdlx"';
        $this->expectExceptionMessage($message);

        $config = new SoapServiceConfiguration('http://gedooo:8080/ODFgedooo/OfficeService?wsdlx');
        $strategy = new SoapStrategy($config);
        $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
    }

    public function testGedoooFusionFailureUnexpectedHttpStatusException()
    {
        $config = new SoapServiceConfiguration(
            'http://gedooo:8080/ODFgedooo/OfficeService?wsdl',
            null,
            null,
            null,
            null,
            201
        );
        $strategy = new SoapStrategy($config);

        $this->expectException(UnexpectedHttpStatusException::class);
        $message = 'Got HTTP status 200 calling http://gedooo:8080/ODFgedooo/OfficeService?wsdl (expected 201)';
        $this->expectExceptionMessage($message);

        $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
    }

    public function testGedoooFusionFailureInvalidTemplateException()
    {
        $this->expectException(InvalidTemplateException::class);
        $this->expectExceptionMessage(sprintf('Incapable de lire le modèle ODT "%s"', __FILE__));

        $config = new SoapServiceConfiguration('http://gedooo:8080/ODFgedooo/OfficeService?wsdl');
        $strategy = new SoapStrategy($config);
        $strategy->fusion(__FILE__, $this->getCommonPartType());
    }

    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testGedoooVersionSuccess(?ServiceProxyConfiguration $proxy)
    {
        $config = new SoapServiceConfiguration('http://gedooo:8080/ODFgedooo/OfficeService?wsdl', null, null, $proxy);
        $strategy = new SoapStrategy($config);
        $actual = $strategy->version();
        $this->assertEquals('1.0.1', $actual);
    }

    public function testGedoooVersionFailureConnectionException()
    {
        $this->expectException(ConnectionException::class);
        $message = 'SOAP-ERROR: Parsing WSDL: Couldn\'t load from \'http://gedooo:8080/ODFgedooo/OfficeService?wsdlx\''
            . ' : failed to load external entity "http://gedooo:8080/ODFgedooo/OfficeService?wsdlx"';
        $this->expectExceptionMessage($message);

        $config = new SoapServiceConfiguration('http://gedooo:8080/ODFgedooo/OfficeService?wsdlx');
        $strategy = new SoapStrategy($config);
        $strategy->version();
    }
}

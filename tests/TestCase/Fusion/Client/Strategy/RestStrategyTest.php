<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Fusion\Client\Strategy;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use Libriciel\OfficeClients\Fusion\Client\Configuration\RestServiceConfiguration;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Fusion\Client\Strategy\RestStrategy;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class RestStrategyTest extends AbstractTestCase
{
    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testFlowFusionSuccess(?ServiceProxyConfiguration $proxy)
    {
        $config = new RestServiceConfiguration('http://flow:8080', null, null, $proxy);
        $strategy = new RestStrategy($config);
        $odtContent = $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
        $actual = $this->saveOdtGetContentXml(__METHOD__, $odtContent);
        $this->assertXmlStringEqualsXmlString($this->getExpectedFlowContentXml(), $actual);
    }

    public function testFlowFusionFailureConnectionException()
    {
        $config = new RestServiceConfiguration('http://flow:80');
        $strategy = new RestStrategy($config);

        $this->expectException(ConnectionException::class);
        $message = 'Could not reach http://flow:80/ODFgedooo/rest/fusion';
        $this->expectExceptionMessage($message);
        $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
    }

    public function testFlowFusionFailureUnexpectedHttpStatusException()
    {
        $config = new RestServiceConfiguration(null, null, null, null, null, 201);
        $strategy = new RestStrategy($config);

        $this->expectException(UnexpectedHttpStatusException::class);
        $message = 'Got HTTP status 200 calling http://flow:8080/ODFgedooo/rest/fusion (expected 201)';
        $this->expectExceptionMessage($message);
        $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
    }

    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testFlowVersionSuccess(?ServiceProxyConfiguration $config)
    {
        $config = new RestServiceConfiguration('http://flow:8080', null, null, $config);
        $strategy = new RestStrategy($config);
        $actual = $strategy->version();
        $this->assertEquals('1.0.0-rc.6', $actual);
    }

    public function testFlowVersionFailureConnectionException()
    {
        $config = new RestServiceConfiguration('http://flow:80');
        $strategy = new RestStrategy($config);

        $this->expectException(ConnectionException::class);
        $message = 'Could not reach http://flow:80/ODFgedooo/rest/version';
        $this->expectExceptionMessage($message);
        $strategy->version();
    }

    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testGedoooFusionSuccess(?ServiceProxyConfiguration $proxy)
    {
        $config = new RestServiceConfiguration('http://gedooo:8080', null, null, $proxy);
        $strategy = new RestStrategy($config);
        $odtContent = $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
        $actual = $this->saveOdtGetContentXml(__METHOD__, $odtContent);
        $this->assertXmlStringEqualsXmlString($this->getExpectedGedoooContentXml(), $actual);
    }

    public function testGedoooFusionFailureConnectionException()
    {
        $config = new RestServiceConfiguration('http://gedooo:80');
        $strategy = new RestStrategy($config);

        $this->expectException(ConnectionException::class);
        $message = 'Could not reach http://gedooo:80/ODFgedooo/rest/fusion';
        $this->expectExceptionMessage($message);
        $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
    }

    public function testGedoooFusionFailureUnexpectedHttpStatusException()
    {
        $config = new RestServiceConfiguration('http://gedooo:8080', null, null, null, null, 201);
        $strategy = new RestStrategy($config);

        $this->expectException(UnexpectedHttpStatusException::class);
        $message = 'Got HTTP status 200 calling http://gedooo:8080/ODFgedooo/rest/fusion (expected 201)';
        $this->expectExceptionMessage($message);
        $strategy->fusion($this->getFixturePath('template.odt'), $this->getCommonPartType());
    }

    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testGedoooVersionSuccess(?ServiceProxyConfiguration $proxy)
    {
        $config = new RestServiceConfiguration('http://gedooo:8080', null, null, $proxy);
        $strategy = new RestStrategy($config);
        $actual = $strategy->version();
        $this->assertEquals('2.1.8-546981', $actual);
    }

    public function testGedoooVersionFailureConnectionException()
    {
        $config = new RestServiceConfiguration('http://gedooo:80');
        $strategy = new RestStrategy($config);

        $this->expectException(ConnectionException::class);
        $message = 'Could not reach http://gedooo:80/ODFgedooo/rest/version';
        $this->expectExceptionMessage($message);
        $strategy->version();
    }
}

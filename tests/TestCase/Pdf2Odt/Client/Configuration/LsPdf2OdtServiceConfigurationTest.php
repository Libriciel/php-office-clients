<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Pdf2Odt\Client\Configuration;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\LsPdf2OdtServiceConfiguration;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class LsPdf2OdtServiceConfigurationTest extends AbstractTestCase
{
    public function testConstructDefaultEnvVars()
    {
        $actual = $this->getObjectAttributes(new LsPdf2OdtServiceConfiguration());
        $this->assertEquals($this->getDefaultLspdf2odtConfiguration(), $actual);
    }

    public function testConstructDefaultEnvVarsServiceProxyConfiguration()
    {
        $config = new LsPdf2OdtServiceConfiguration(
            null,
            null,
            new ServiceProxyConfiguration(
                'host_object',
                0,
                'username_object',
                'password_object'
            ),
            null,
            null,
            'ghostscript',
            150
        );
        $actual = $this->getObjectAttributes($config);
        $expected = array_merge(
            $this->getDefaultLspdf2odtConfiguration(),
            [
                'proxy' => [
                    'host' => 'host_object',
                    'port' => 0,
                    'username' => 'username_object',
                    'password' => 'password_object',
                ],
            ]
        );
        $this->assertEquals($expected, $actual);
    }

    public function testConstructOverloadedConstructor()
    {
        $actual = $this->getObjectAttributes(
            new LsPdf2OdtServiceConfiguration(
                'baseUri_constructor',
                'path_constructor',
                new ServiceProxyConfiguration(
                    'host_constructor',
                    123,
                    'username_constructor',
                    'password_constructor'
                ),
                null,
                null,
                'method_constructor',
                666
            )
        );
        $expected = [
            'baseUri' => 'baseUri_constructor',
            'path' => 'path_constructor',
            'method' => 'method_constructor',
            'resolution' => 666,
            'proxy' => [
                'host' => 'host_constructor',
                'port' => 123,
                'username' => 'username_constructor',
                'password' => 'password_constructor',
            ],
            'verbose' => false,
            'expectedHttpStatus' => 201,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testConstructOverloadedEnvVars()
    {
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_BASE_URI=baseUri_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PATH=path_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_METHOD=method_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_RESOLUTION=666');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY=1');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY_HOST=host_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY_PORT=456');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY_USERNAME=username_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_PROXY_PASSWORD=password_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_VERBOSE=1');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT_DEFAULT_EXPECTED_HTTP_STATUS=202');

        $actual = $this->getObjectAttributes(new LsPdf2OdtServiceConfiguration());
        $expected = [
            'baseUri' => 'baseUri_env',
            'path' => 'path_env',
            'proxy' => [
                'host' => 'host_env',
                'port' => 456,
                'username' => 'username_env',
                'password' => 'password_env',
            ],
            'verbose' => true,
            'expectedHttpStatus' => 202,
            'method' => 'method_env',
            'resolution' => 666
        ];
        $this->assertEquals($expected, $actual);
    }
}

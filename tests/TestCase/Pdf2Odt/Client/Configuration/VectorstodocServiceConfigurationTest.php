<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Pdf2Odt\Client\Configuration;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\VectorstodocServiceConfiguration;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class VectorstodocServiceConfigurationTest extends AbstractTestCase
{
    public function testConstructDefaultEnvVars()
    {
        $actual = $this->getObjectAttributes(new VectorstodocServiceConfiguration());
        $this->assertEquals($this->getDefaultVectorstodocConfiguration(), $actual);
    }

    public function testConstructDefaultEnvVarsServiceProxyConfiguration()
    {
        $config = new VectorstodocServiceConfiguration(
            null,
            null,
            new ServiceProxyConfiguration(
                'host_object',
                0,
                'username_object',
                'password_object'
            ),
            null,
            null
        );
        $actual = $this->getObjectAttributes($config);
        $expected = array_merge(
            $this->getDefaultVectorstodocConfiguration(),
            [
                'proxy' => [
                    'host' => 'host_object',
                    'port' => 0,
                    'username' => 'username_object',
                    'password' => 'password_object',
                ],
            ]
        );
        $this->assertEquals($expected, $actual);
    }

    public function testConstructOverloadedConstructor()
    {
        $actual = $this->getObjectAttributes(
            new VectorstodocServiceConfiguration(
                'baseUri_constructor',
                'path_constructor',
                new ServiceProxyConfiguration(
                    'host_constructor',
                    123,
                    'username_constructor',
                    'password_constructor'
                ),
                null,
                null
            )
        );
        $expected = [
            'baseUri' => 'baseUri_constructor',
            'path' => 'path_constructor',
            'proxy' => [
                'host' => 'host_constructor',
                'port' => 123,
                'username' => 'username_constructor',
                'password' => 'password_constructor',
            ],
            'verbose' => false,
            'expectedHttpStatus' => 200,
        ];
        $this->assertEquals($expected, $actual);
    }

    public function testConstructOverloadedEnvVars()
    {
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_BASE_URI=baseUri_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PATH=path_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY=1');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY_HOST=host_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY_PORT=456');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY_USERNAME=username_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_PROXY_PASSWORD=password_env');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_VERBOSE=1');
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC_DEFAULT_EXPECTED_HTTP_STATUS=202');

        $actual = $this->getObjectAttributes(new VectorstodocServiceConfiguration());
        $expected = [
            'baseUri' => 'baseUri_env',
            'path' => 'path_env',
            'proxy' => [
                'host' => 'host_env',
                'port' => 456,
                'username' => 'username_env',
                'password' => 'password_env',
            ],
            'verbose' => true,
            'expectedHttpStatus' => 202
        ];
        $this->assertEquals($expected, $actual);
    }
}

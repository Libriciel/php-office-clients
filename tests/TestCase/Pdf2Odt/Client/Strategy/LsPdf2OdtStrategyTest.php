<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Pdf2Odt\Client\Strategy;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\LsPdf2OdtServiceConfiguration;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\LsPdf2OdtStrategy;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class LsPdf2OdtStrategyTest extends AbstractTestCase
{
    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testPdf2OdtSuccess(?ServiceProxyConfiguration $proxy)
    {
        $config = new LsPdf2OdtServiceConfiguration(null, null, $proxy);
        $strategy = new LsPdf2OdtStrategy($config);
        $odtContent = $strategy->pdf2odt($this->getFixturePath('2_pages.pdf'));
        $actual = $this->saveOdtGetPicturesList(__METHOD__, $odtContent);
        $expected = ['page_00001.png', 'page_00002.png'];
        $this->assertEquals($expected, $actual);
    }

    public function testPdf2OdtFailureConnectionException()
    {
        $config = new LsPdf2OdtServiceConfiguration('http://lspdf2odt:11');
        $strategy = new LsPdf2OdtStrategy($config);

        $this->expectException(ConnectionException::class);
        $message = 'Could not reach http://lspdf2odt:11';
        $this->expectExceptionMessage($message);
        $strategy->pdf2odt($this->getFixturePath('2_pages.pdf'));
    }

    public function testPdf2OdtFailureUnexpectedHttpStatusException()
    {
        $config = new LsPdf2OdtServiceConfiguration(null, null, null, null, 200);
        $strategy = new LsPdf2OdtStrategy($config);

        $this->expectException(UnexpectedHttpStatusException::class);
        $pattern = 'Got HTTP status 201 calling http://lspdf2odt/api/v1/pdf2odt (expected 200)';
        $this->expectExceptionMessageMatches('/' . preg_quote($pattern, '/') . '/');
        $strategy->pdf2odt($this->getFixturePath('2_pages.pdf'));
    }
}

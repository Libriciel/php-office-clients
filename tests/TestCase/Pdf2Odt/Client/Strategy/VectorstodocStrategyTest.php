<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Pdf2Odt\Client\Strategy;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;
use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\VectorstodocServiceConfiguration;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\VectorstodocStrategy;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class VectorstodocStrategyTest extends AbstractTestCase
{
    /**
     * @dataProvider dataProxyConfigurationSuccess
     */
    public function testVectorstodocSuccess(?ServiceProxyConfiguration $proxy)
    {
        $this->markTestSkipped('En attente de la version suivante de vectorstodoc');
        $config = new VectorstodocServiceConfiguration(null, null, $proxy);
        $strategy = new VectorstodocStrategy($config);
        $odtContent = $strategy->pdf2odt($this->getFixturePath('2_pages.pdf'));
        $actual = $this->saveOdtGetPicturesList(__METHOD__, $odtContent);
        $expected = ['page_00001.png', 'page_00002.png'];
        $this->assertEquals(count($expected), count($actual));
    }

    public function testVectorstodocFailureConnectionException()
    {
        $this->markTestSkipped('En attente de la version suivante de vectorstodoc');
        $config = new VectorstodocServiceConfiguration('http://vectorstodoc:11');
        $strategy = new VectorstodocStrategy($config);

        $this->expectException(ConnectionException::class);
        $message = 'Could not reach http://vectorstodoc:11';
        $this->expectExceptionMessage($message);
        $strategy->pdf2odt($this->getFixturePath('2_pages.pdf'));
    }

    public function testVectorstodocFailureUnexpectedHttpStatusException()
    {
        $this->markTestSkipped('En attente de la version suivante de vectorstodoc');
        $config = new VectorstodocServiceConfiguration(null, null, null, null, 202);
        $strategy = new VectorstodocStrategy($config);

        $this->expectException(UnexpectedHttpStatusException::class);
        $pattern = 'Got HTTP status 200 calling http://vectorstodoc:8080/api/v1/pdf-to-odt (expected 202)';
        $this->expectExceptionMessageMatches('/' . preg_quote($pattern, '/') . '/');
        $strategy->pdf2odt($this->getFixturePath('2_pages.pdf'));
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Test\TestCase\Pdf2Odt\Client;

use Libriciel\OfficeClients\Pdf2Odt\Client\ClientFactory;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\LsPdf2OdtServiceConfiguration;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\RestServiceConfiguration;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\SoapServiceConfiguration;
use Libriciel\OfficeClients\Exception\InvalidStrategyException;
use Libriciel\OfficeClients\Test\AbstractTestCase;

class ClientFactoryTest extends AbstractTestCase
{
    public function testCreateDefaultStrategySuccess()
    {
        $actual = ClientFactory::create();

        $this->assertInstanceOf('\Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\LsPdf2OdtStrategy', $actual);
        $this->assertEquals($this->getDefaultLspdf2odtConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateOverloadedEnvVarStrategySuccess()
    {
        putenv('PHP_OFFICE_CLIENTS_PDF2ODT_DEFAULT_STRATEGY=lspdf2odt');
        $actual = ClientFactory::create();

        $this->assertInstanceOf('\Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\LsPdf2OdtStrategy', $actual);
        $this->assertEquals($this->getDefaultLspdf2odtConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateLspdf2odtStrategyDefaultConfigurationSuccess()
    {
        $actual = ClientFactory::create('lspdf2odt');

        $this->assertInstanceOf('\Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\LsPdf2OdtStrategy', $actual);
        $this->assertEquals($this->getDefaultLspdf2odtConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateVectorstodocStrategyDefaultConfigurationSuccess()
    {
        $actual = ClientFactory::create('vectorstodoc');

        $this->assertInstanceOf('\Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\VectorstodocStrategy', $actual);
        $this->assertEquals($this->getDefaultVectorstodocConfiguration(), $this->exportConfig($actual));
    }

    public function testCreateUnknownStrategyFailure()
    {
        $this->expectException(InvalidStrategyException::class);
        $this->expectExceptionMessage('Invalid strategy "unknown" requested, use one of "lspdf2odt", "vectorstodoc".');

        ClientFactory::create('unknown');
    }

    public function testCreateLspdf2odtStrategyCustomConfigurationSuccess()
    {
        $actual = ClientFactory::create('lspdf2odt', new LsPdf2OdtServiceConfiguration('http://otherlspdf2odt:1234'));

        $this->assertInstanceOf('\Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\LsPdf2OdtStrategy', $actual);
        $expected = array_merge($this->getDefaultLspdf2odtConfiguration(), ['baseUri' => 'http://otherlspdf2odt:1234']);
        $this->assertEquals($expected, $this->exportConfig($actual));
    }
}

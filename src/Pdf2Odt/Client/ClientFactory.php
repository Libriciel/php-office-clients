<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Pdf2Odt\Client;

use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\ServiceConfigurationInterface;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\LsPdf2OdtServiceConfiguration;
use Libriciel\OfficeClients\Exception\InvalidStrategyException;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\VectorstodocServiceConfiguration;
use Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\LsPdf2OdtStrategy;
use Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\StrategyInterface;
use Libriciel\OfficeClients\Pdf2Odt\Client\Strategy\VectorstodocStrategy;

abstract class ClientFactory
{
    public const LSPDF2ODT = 'lspdf2odt';
    public const VECTORSTODOC = 'vectorstodoc';

    public static function create(
        ?string $strategy = null,
        ?ServiceConfigurationInterface $config = null
    ): StrategyInterface {
        $strategy = $strategy ?: (getenv('PHP_OFFICE_CLIENTS_PDF2ODT_DEFAULT_STRATEGY') ?: static::LSPDF2ODT);

        switch ($strategy) {
            case static::LSPDF2ODT:
                return new LsPdf2OdtStrategy($config ?: (new LsPdf2OdtServiceConfiguration()));
            case static::VECTORSTODOC:
                return new VectorstodocStrategy($config ?: (new VectorstodocServiceConfiguration()));
            default:
                $format = 'Invalid strategy "%s" requested, use one of "lspdf2odt", "vectorstodoc".';
                throw new InvalidStrategyException(sprintf($format, $strategy));
        }
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Pdf2Odt\Client\Strategy;

use GuzzleHttp\Psr7;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\LsPdf2OdtServiceConfiguration;

class LsPdf2OdtStrategy extends AbstractStrategy
{
    /**
     * @var LsPdf2OdtServiceConfiguration
     */
    protected $config;

    public function __construct(LsPdf2OdtServiceConfiguration $config)
    {
        $this->config = $config;
    }

    protected function getRequestOptions(string $path, $resolution = null, $method = null): array
    {
        return [
            'http_errors' => false,
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => Psr7\Utils::tryFopen($path, 'r'),
                ],
                [
                    'name' => 'resolution',
                    'contents' => $resolution !== null ? $resolution : $this->config->resolution,
                ],
                [
                    'name' => 'method',
                    'contents' => $method !== null ? $method : $this->config->method,
                ],
            ],
        ];
    }
}

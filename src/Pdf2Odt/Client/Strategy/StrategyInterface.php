<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Pdf2Odt\Client\Strategy;

interface StrategyInterface
{
    public function pdf2odt(string $path, $resolution = null, $method = null): string;
}

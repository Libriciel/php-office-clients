<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Pdf2Odt\Client\Strategy;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;

abstract class AbstractStrategy implements StrategyInterface
{
    /**
     * @var \Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\LsPdf2OdtServiceConfiguration|\Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\VectorstodocServiceConfiguration
     */
    protected $config;

    abstract protected function getRequestOptions(string $path, $resolution = null, $method = null): array;

    public function pdf2odt(string $path, $resolution = null, $method = null): string
    {
        $url = $this->config->baseUri . $this->config->path;
        $client = new Client();

        $options = $this->getRequestOptions($path, $resolution, $method);

        if ($this->config->proxy !== null) {
            $pattern = '/^(https{0,1}):\/\/(.*)$/i';
            $scheme = preg_replace($pattern, '\1', $this->config->proxy->host);
            $host = preg_replace($pattern, '\2', $this->config->proxy->host);
            $options['proxy'] = [
                $scheme => sprintf(
                    '%s://%s:%s@%s:%d',
                    $scheme,
                    $this->config->proxy->username,
                    $this->config->proxy->password,
                    $host,
                    $this->config->proxy->port
                )
            ];
        }

        try {
            $response = $client->request('POST', $url, $options);
        } catch (ConnectException $exc) {
            throw new ConnectionException(sprintf('Could not reach %s', $url, 0, $exc));
        }

        if ($response->getStatusCode() === 0) {
            throw new ConnectionException(sprintf('Could not reach %s', $url));
        }

        if ($this->config->expectedHttpStatus !== null) {
            if ($response->getStatusCode() !== $this->config->expectedHttpStatus) {
                $message = sprintf(
                    "Got HTTP status %d calling %s (expected %s)",
                    $response->getStatusCode(),
                    $url,
                    $this->config->expectedHttpStatus
                );
                throw new UnexpectedHttpStatusException($message, (string)$response->getBody());
            }
        }

        return (string)$response->getBody();
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Pdf2Odt\Client\Strategy;

use GuzzleHttp\Psr7;
use Libriciel\OfficeClients\Pdf2Odt\Client\Configuration\VectorstodocServiceConfiguration;

class VectorstodocStrategy extends AbstractStrategy
{
    /**
     * @var VectorstodocServiceConfiguration
     */
    protected $config;

    public function __construct(VectorstodocServiceConfiguration $config)
    {
        $this->config = $config;
    }

    protected function getRequestOptions(string $path, $resolution = null, $method = null): array
    {
        return [
            'http_errors' => false,
            'multipart' => [
                [
                    'name' => 'pdf',
                    'contents' => Psr7\Utils::tryFopen($path, 'r'),
                ],
            ],
        ];
    }
}

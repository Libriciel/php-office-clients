<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Pdf2Odt\Client\Configuration;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;

class LsPdf2OdtServiceConfiguration extends AbstractServiceConfiguration
{
    protected const ENV_PREFIX = 'PHP_OFFICE_CLIENTS_PDF2ODT_LSPDF2ODT';

    protected const DEFAULT_BASE_URI = 'http://lspdf2odt';
    protected const DEFAULT_PATH = '/api/v1/pdf2odt';
    protected const DEFAULT_EXPECTED_HTTP_STATUS = 201;
    protected const DEFAULT_METHOD = 'ghostscript';
    protected const DEFAULT_RESOLUTION = 150;

    public string $method;
    public int $resolution;

    public function __construct(
        ?string $baseUri = null,
        ?string $path = null,
        ?ServiceProxyConfiguration $proxy = null,
        ?bool $verbose = null,
        ?int $expectedHttpStatus = null,
        ?string $method = null,
        ?int $resolution = null
    ) {
        parent::__construct($baseUri, $path, $proxy, $verbose, $expectedHttpStatus);

        $prefix = static::ENV_PREFIX . '_DEFAULT_';

        $this->method = $this->getString($method, $prefix . 'METHOD', static::DEFAULT_METHOD);
        $this->resolution = $this->getInt($resolution, $prefix . 'RESOLUTION', static::DEFAULT_RESOLUTION);
    }
}

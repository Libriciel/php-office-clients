<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Pdf2Odt\Client\Configuration;

class VectorstodocServiceConfiguration extends AbstractServiceConfiguration
{
    protected const ENV_PREFIX = 'PHP_OFFICE_CLIENTS_PDF2ODT_VECTORSTODOC';

    protected const DEFAULT_BASE_URI = 'http://vectorstodoc:8080';
    protected const DEFAULT_PATH = '/api/v1/pdf-to-odt';
    protected const DEFAULT_EXPECTED_HTTP_STATUS = 200;
}

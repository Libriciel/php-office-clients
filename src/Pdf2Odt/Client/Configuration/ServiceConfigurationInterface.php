<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Pdf2Odt\Client\Configuration;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;

interface ServiceConfigurationInterface
{
    public function __construct(
        ?string $baseUri = null,
        ?string $path = null,
        ?ServiceProxyConfiguration $proxy = null,
        ?bool $verbose = null,
        int $expectedHttpStatus = null
    );
}

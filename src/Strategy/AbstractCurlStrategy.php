<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Strategy;

abstract class AbstractCurlStrategy
{
    protected function getBaseCurlOptions()
    {
        $options = [
            CURLOPT_HTTPHEADER => [
                'Expect:',
            ],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_VERBOSE => $this->config->verbose,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        ];

        if ($this->config->proxy !== null) {
            $options[CURLOPT_PROXY] = $this->config->proxy->host . ':' . $this->config->proxy->port;
            $options[CURLOPT_PROXYUSERPWD] = $this->config->proxy->username . ':' . $this->config->proxy->password;
        }

        return $options;
    }
}

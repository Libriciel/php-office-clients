<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Exception;

use RuntimeException;
use Throwable;

class UnexpectedHttpStatusException extends RuntimeException
{
    protected ?string $response;

    public function __construct(string $message, string $response, Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
        $this->response = $response;
    }
}

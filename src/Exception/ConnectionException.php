<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Exception;

use RuntimeException;

class ConnectionException extends RuntimeException
{
}

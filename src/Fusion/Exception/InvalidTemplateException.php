<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Exception;

use RuntimeException;

class InvalidTemplateException extends RuntimeException
{
}

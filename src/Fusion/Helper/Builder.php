<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Helper;

use Libriciel\OfficeClients\Fusion\Type\ContentType;
use Libriciel\OfficeClients\Fusion\Type\FieldType;
use Libriciel\OfficeClients\Fusion\Type\IterationType;
use Libriciel\OfficeClients\Fusion\Type\PartType;

class Builder
{
    public const MIME_ODT = 'application/vnd.oasis.opendocument.text';

    public const TYPE_DATE = 'date';
    public const TYPE_LINES = 'lines';
    public const TYPE_NUMBER = 'number';
    public const TYPE_STRING = 'string';
    public const TYPE_TEXT = 'text';

    protected PartType $result;

    public function __construct(?PartType $part = null)
    {
        $this->result = $part === null
            ? new PartType()
            : $part;
    }

    public function addIteration(string $name, array $parts)
    {
        $section = new IterationType($name);
        foreach ($parts as $part) {
            $section->addPart($part);
        }
        $this->result->addElement($section);
        return $this;
    }

    public function addContent(string $target, string $name, ?string $value, string $mime): Builder
    {
        $this->result->addElement(new ContentType($target, $name, $mime, 'binary', $value));
        return $this;
    }

    public function addDateField(string $target, ?string $value): Builder
    {
        return $this->addField($target, $value, static::TYPE_DATE);
    }

    public function addField(string $target, $value, string $type = self::TYPE_TEXT): Builder
    {
        $value = is_string($value)
            ? mb_convert_encoding($value, 'UTF-8', mb_detect_encoding($value))
            : $value;
        $this->result->addElement(new FieldType($target, $value, $type));
        return $this;
    }

    public function addLinesField(string $target, ?string $value): Builder
    {
        return $this->addField($target, $value, static::TYPE_LINES);
    }

    public function addNumberField(string $target, $value): Builder
    {
        return $this->addField($target, $value, static::TYPE_NUMBER);
    }

    public function addOdtContent(string $target, string $name, ?string $value): Builder
    {
        $this->result->addElement(new ContentType($target, $name, static::MIME_ODT, 'binary', $value));
        return $this;
    }

    public function addStringField(string $target, ?string $value): Builder
    {
        return $this->addField($target, $value, static::TYPE_STRING);
    }

    public function addTextField(string $target, ?string $value): Builder
    {
        return $this->addField($target, $value, static::TYPE_TEXT);
    }

    public function getPart(): PartType
    {
        return $this->result;
    }
}

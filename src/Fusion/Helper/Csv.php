<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Helper;

use Libriciel\OfficeClients\Fusion\Type\FieldType;
use Libriciel\OfficeClients\Fusion\Type\IterationType;
use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * Classe Csv
 *
 * Un objet de type Csv gère les fichiers au format CSV
 * et les convertit en iteration pour la fusion
 */
class Csv
{
    public $iterationName;
    public $column2Field;
    public $column2Type;
    public $csvFileName;
    public $fieldSeparator;
    public $stringDelimiter;
    public $encoding;
    public $constantValues;
    public $constantTypes;

    public function __construct($iterationName)
    {
        $this->iterationName = $iterationName;
        $this->column2Field = [];
        $this->constantValues = [];
        $this->constantTypes = [];
    }

    public function setCSVFile($filename, $fieldSeparator, $stringDelimiter, $encoding)
    {
        $this->csvFileName = $filename;
        $this->fieldSeparator = $fieldSeparator;
        $this->stringDelimiter = $stringDelimiter;
        $this->encoding = strtolower($encoding);
    }

    public function mapField($columnName, $fieldName, $type = "default")
    {
        if (trim($type) == "") {
            $type = "vide";
        }
        $this->column2Field[strtolower(trim($columnName))] = trim($fieldName);
        $this->column2Type[strtolower(trim($columnName))] = strtolower(trim($type));
    }

    /*
     * Si les noms de colonnes ne correspondent pas au noms de champs un fichier de mapping peut etre utilisé...
     */

    public function setMapFile($filename)
    {

        if ($filename == "") {
            return;
        }

        $handle = fopen($filename, 'r');
        if ($handle == null) {
            // Couldn't open/read from CSV file.
            return;
        }

        while (($data = fgetcsv($handle, 1000, ";", "\"")) !== false) {
            $this->mapField($data[0], $data[1], $data[2]);
        }
    }

    /*
     * .. ou bien un tableau.
     */

    public function setMapArray($mapArray)
    {
        $this->column2Field = $mapArray;
    }

    /*
     * Ajout d'une constante à répéter à chaque itération
     */

    public function addConstant($name, $value, $type = "string")
    {
        if ($type == "") {
            $type = "string";
        }
        $this->constantValues[$name] = $value;
        $this->constantTypes[$name] = $type;
    }

    public function getIteration()
    {
        // Couldn't open CSV file.
        $handle = fopen($this->csvFileName, 'r');
        if (empty($handle)) {
            return null;
        }

        // Couldn't read from CSV file.
        $data = fgetcsv($handle, 5000, $this->fieldSeparator, $this->stringDelimiter);
        if ($data === false) {
            return null;
        }

        //
        // Getting columnNames
        //
        $names = [];
        foreach ($data as $field) {
            $names[] = strtolower(trim($field));
        }

        // Setting columns indexes
        $index = [];
        $iCol = 0;
        foreach ($names as $name) {
            $index[$name] = $iCol;
            $iCol++;
        }

        // Setting default map, if necessary.
        if (count($this->column2Field) == 0) {
            foreach ($names as $name) {
                $this->mapField($name, $name);
            }
        }

        // Creating iteration

        $oIteration = new IterationType($this->iterationName);
        while (
            ($data = fgetcsv(
                $handle,
                5000,
                $this->fieldSeparator,
                $this->stringDelimiter
            )) !== false
        ) {
            $oPart = new PartType();
            foreach ($this->constantValues as $name => $value) {
                if ($this->encoding != "utf8") {
                    $value = utf8_encode($value);
                }
                $oPart->addElement(new FieldType($name, $value, $this->constantTypes[$name]));
            }
            foreach ($this->column2Field as $column => $field) {
                if (!isset($this->constantValue[$field])) {
                    $value = $data[$index[$column]];
                    if ($this->encoding != "utf8") {
                        $value = utf8_encode($value);
                    }

                    $type = $this->column2Type[$column];
                    $oPart->addElement(new FieldType($field, $value, $type));
                }
            }
            $oIteration->addPart($oPart);
        }

        return $oIteration;
    }
}

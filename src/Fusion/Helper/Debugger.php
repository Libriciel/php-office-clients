<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Helper;

use Libriciel\OfficeClients\Fusion\Type\PartType;

/**
 * La classe Debugger permet d'exporter récursivement les informations d'un PartType et de ses parties field,
 * iteration et content au format CSV.
 */
abstract class Debugger
{
    /**
     * CSV separator.
     *
     * @var string
     */
    public static $csvSeparator = ',';
    /**
     * Variable path key parts separator
     *
     * @var string
     */
    public static $pathSeparator = ',';

    /**
     * Title line used by self::allPathsToCsv().
     *
     * @var array
     */
    public static $allPathsToCsvTitles = array(
        'Chemins',
        'Types',
        'Valeurs'
    );

    /**
     * Title line used by self::hashPathsToCsv().
     *
     * @var array
     */
    public static $hashPathsToCsvTitles = array(
        'Chemins',
        'Types'
    );

    /**
     * Ables to know which "hash" path are already found if only hash
     * path must be exported
     *
     * @see allPathsToCsv(), toArray(), hashPathsToCsv(), ...
     * @var array
     */
    protected static $hashPaths = array();

    /**
     * Ables to know if all paths must be exported or
     * only the hash ones
     *
     * @see allPathsToCsv(), toArray(), hashPathsToCsv()
     * @var boolean
     */
    protected static $returnHashPaths = false;

    /**
     * PAbles to know if fields values or files md5 must be exported
     * since self::allPathsToCsv() call.
     *
     *
     * @see self::gdoFieldTypeToArray(), self::gdoContentTypeToArray()
     * @var boolean
     */
    protected static $exportValues = false;

    /**
     * Ables to know if a column that translates fields name is wanted
     *
     * @see allPathsToCsv(), hashPathsToCsv(), lineToCsv()
     * @var boolean
     */
    protected static $translateField = false;

    /**
     * Returns field's key from path parts.
     *
     * @param array $parts
     * @return string
     */
    protected static function keyPath(array $parts)
    {
        return implode('.', array_filter($parts, function ($value) {
            return !empty($value) || (string)$value === '0';
        }));
    }

    /**
     * GDO_FieldType treatments of $part->field.
     *
     * @param PartType $part
     * @param string $iterationName
     * @param integer $iterationNumber
     * @return array
     */
    protected static function gdoFieldTypeToArray(PartType $part, $iterationName = null, $iterationNumber = null)
    {
        $return = array();

        if (isset($part->field) && !empty($part->field)) {
            foreach ($part->field as $field) {
                $keyPath = self::keyPath(array($iterationName, $iterationNumber));
                $line = array();

                if (self::$returnHashPaths) {
                    $sectionPath = self::hashPath(self::keyPath([$iterationName, $iterationNumber, $field->target]));
                    if (!in_array($sectionPath, self::$hashPaths)) {
                        self::$hashPaths[] = $sectionPath;
                        $line = array($sectionPath, $field->dataType);
                    }
                } else {
                    $keyPath = self::keyPath(array($iterationName, $iterationNumber, $field->target));
                    $line = array($keyPath, $field->dataType);
                    if (self::$exportValues) {
                        $line[] = $field->value;
                    }
                }

                if (!empty($line)) {
                    $return[] = self::lineToCsv($line);
                }
            }
        }

        return $return;
    }

    /**
     * GDO_IterationType treatments of $part->iteration.
     *
     * @param PartType $part
     * @param string $iterationName
     * @param integer $iterationNumber
     * @return array
     */
    protected static function gdoIterationTypeToArray(
        PartType $part,
        $iterationName = null,
        $iterationNumber = null
    ) {
        $return = array();

        if (isset($part->iteration) && !empty($part->iteration)) {
            foreach ($part->iteration as $iteration) {
                foreach ($iteration->part as $i => $part) {
                    $keyPath = self::keyPath(array($iterationName, $iterationNumber, $iteration->name));
                    $return = array_merge($return, self::toArray($part, $keyPath, $i));
                }
            }
        }

        return $return;
    }

    /**
     * GDO_ContentType treatments of $part->content.
     *
     * @param PartType $part
     * @param string $iterationName
     * @param integer $iterationNumber
     * @return array
     *
     */
    protected static function gdoContentTypeToArray(PartType $part, $iterationName = null, $iterationNumber = null)
    {
        $return = array();

        if (isset($part->content) && !empty($part->content)) {
            foreach ($part->content as $content) {
                $keyPath = self::keyPath(array($iterationName, $iterationNumber, $content->name));
                $line = array();

                if (self::$returnHashPaths) {
                    $sectionPath = self::hashPath($keyPath);
                    if (!in_array($sectionPath, self::$hashPaths)) {
                        self::$hashPaths[] = $sectionPath;
                        $line = array($sectionPath, $content->mimeType);
                    }
                } else {
                    $line = array($keyPath, $content->mimeType, $content->target);
                    if (self::$exportValues) {
                        $line[] = md5($content->binary);
                    }
                }

                if (!empty($line)) {
                    $return[] = self::lineToCsv($line);
                }
            }
        }

        return $return;
    }

    /**
     * GDO_FieldType, GDO_IterationType and GDO_ContentType treatments
     * of a PartType.
     *
     * The other ones can easily be added in a sub-class
     *
     * @param PartType $part
     * @param string $iterationName
     * @param integer $iterationNumber
     * @return array
     */
    protected static function toArray(PartType $part, $iterationName = null, $iterationNumber = null)
    {
        return array_merge(
            self::gdoFieldTypeToArray($part, $iterationName, $iterationNumber),
            self::gdoIterationTypeToArray($part, $iterationName, $iterationNumber),
            self::gdoContentTypeToArray($part, $iterationName, $iterationNumber)
        );
    }

    /**
     * Returns info for every paths, in CSV format
     *
     * @param PartType $part
     * @param boolean $exportValues
     * @return string
     */
    public static function allPathsToCsv(PartType $part, $exportValues = false)
    {
        // Initialisation
        self::$hashPaths = array();
        self::$returnHashPaths = false;
        self::$exportValues = $exportValues;

        $lines = self::toArray($part);
        $titles = self::$allPathsToCsvTitles;
        if (!self::$exportValues) {
            array_pop($titles);
        }
        array_unshift($lines, self::lineToCsv($titles));
        return implode("\n", $lines);
    }

    /**
     * Returns info for hash paths, in CSV format
     *
     * @param PartType $part
     * @return string
     */
    public static function hashPathsToCsv(PartType $part)
    {
        // Initialisation
        self::$hashPaths = array();
        self::$returnHashPaths = true;
        self::$exportValues = false;

        $lines = self::toArray($part);
        array_unshift($lines, self::lineToCsv(self::$hashPathsToCsvTitles));
        return implode("\n", $lines);
    }

    /**
     * Returns hash path from a path.
     *
     * @param string $sectionPath
     * @return string
     */
    protected static function hashPath($sectionPath)
    {
        if (!empty($sectionPath)) {
            $sectionPath = preg_replace('/\.[0-9]+/', '.{n}', $sectionPath);
        }

        return $sectionPath;
    }

    /**
     * Transforms a values array into one line in CSV format, which fields are
     * escaped. The used separator will be self::$csvSeparator
     *
     * @param array $line
     * @return string
     */
    protected static function lineToCsv(array $line)
    {
        foreach (array_keys($line) as $index) {
            $line[$index] = '"' . trim(addslashes($line[$index])) . '"';
        }
        return implode(self::$csvSeparator, $line);
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Helper;

use Libriciel\OfficeClients\Fusion\Type\ContentType;
use Libriciel\OfficeClients\Fusion\Type\FieldType;
use Libriciel\OfficeClients\Fusion\Type\IterationType;
use Libriciel\OfficeClients\Fusion\Type\PartType;
use Libriciel\OfficeClients\Fusion\Utility\Hash;

/**
 * @deprecated
 */
abstract class Director
{
    protected static function keyExists(string $newKey, string $alias, array &$data): bool
    {
        $aliasSplit = explode('.', $newKey);
        $count = count($aliasSplit);

        return (
            ($count == 1 && array_key_exists($alias, $data))
            || (
                $count == 2 && array_key_exists($aliasSplit[0], $data)
                && array_key_exists($aliasSplit[1], $data[$aliasSplit[0]])
            ) || Hash::check($data, $alias) // INFO: Hash::check doesn't work very well if value is null
        );
    }

    /**
     * Builds pdf file by analyzing if values are files or texts, in a PartType
     *
     * @param PartType $part Foo
     * @param array $data Foo
     * @param array $types Foo
     * @param array $correspondances Foo
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    public static function main(PartType $part, array $data, array $types, array $correspondances)
    {
        foreach ($correspondances as $newKey => $alias) {
            if (static::keyExists($newKey, $alias, $data)) {
                $value = Hash::get($data, $alias);
                $type = $types[$newKey] ?? 'text';

                if ($type == 'file') {
                    $part->addElement(
                        new ContentType(
                            $alias,
                            $alias . '.odt',
                            'application/vnd.oasis.opendocument.text',
                            'binary',
                            $value
                        )
                    );
                } else {
                    $value = is_string($value)
                        ? mb_convert_encoding($value, 'UTF-8', mb_detect_encoding($value))
                        : $value;
                    $part->addElement(new FieldType($newKey, $value, $type));
                }
            }
        }

        return $part;
    }

    /**
     * Adds new content to the $part already built above.
     *
     * @param PartType $main Foo
     * @param string $iterationName Foo
     * @param array $datas Foo
     * @param array $types Foo
     * @param array $correspondances Foo
     * @return \Libriciel\OfficeClients\Fusion\Type\PartType
     */
    public static function iteration(
        PartType $main,
        string $iterationName,
        array $datas,
        array $types,
        array $correspondances
    ) {
        $iteration = new IterationType($iterationName);
        foreach ($datas[$iterationName] as $data) {
            $innerPart = new PartType();
            $innerPart = self::main($innerPart, $data, $types, $correspondances);
            foreach ($data as $keyIteration => $value) {
                if (is_array($value)) {
                    self::iteration($innerPart, $keyIteration, [$keyIteration => $value], $types, $correspondances);
                }
            }
            $iteration->addPart($innerPart);
        }

        $main->addElement($iteration);

        return $main;
    }
}

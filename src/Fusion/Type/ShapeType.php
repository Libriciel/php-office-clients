<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

/**
 * Classe ShapeType
 *
 * Un ShapeType est un objet servant à changer le style d'une figure graphique dans un dessin inséré dans le document.
 */
class ShapeType
{
    public $name;
    public $style;
    public $text;

    /**
     * Constructeur
     *
     * @param string $name Non de a figure
     * @param string $style nom du style  à affecter
     */
    public function __construct($name, $style, $text)
    {
        $this->name = $name;
        $this->style = $style;
        $this->text = $text;
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

/**
 * Classe PartType
 *
 * Un objet de type PartType contient les données à insérer dans document.
 * Ces données peuvent être implémentées sous forme de FieldType, de ContenType ou d'IterationType.
 */
class PartType
{
//
// Les propriétés de l'objet vont rester non initialisées
// jusqu'à ce qu'on appelle la fonction "finish".
//
// L'object est de n'initaliser que les propriétés qui contiennent
// au moins un élément.

    public $field;
    public $matrix;
    public $content;
    public $drawing;
    public $iteration;

    /**
     * Ajoute un element à une Part
     *
     * @param string  l'objet à ajouter, qui peut être de type FieldType, ContentType our IterationType
     */
    public function addElement($obj)
    {

        switch (get_class($obj)) {
            case 'Libriciel\OfficeClients\Fusion\Type\FieldType':
                $this->field[] = $obj;
                break;
            case 'Libriciel\OfficeClients\Fusion\Type\MatrixType':
                $this->matrix[] = $obj;
                break;
            case 'Libriciel\OfficeClients\Fusion\Type\ContentType':
                $this->content[] = $obj;
                break;
            case 'Libriciel\OfficeClients\Fusion\Type\DrawingType':
                $this->drawing[] = $obj;
                break;
            case 'Libriciel\OfficeClients\Fusion\Type\IterationType':
                $this->iteration[] = $obj;
                break;
        }
    }

    /**
     * Initialise les propriétés  publiques
     * Cette fonction doit être appelée quand tous les element ont été insérés
     * dans la Part.
     *
     * @return   object  la Part elle-même pour qu'elle puisse être insérée dans le document.
     */
    public function finish()
    {

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

use Exception;
use stdClass;
use SoapClient;

/**
 * Classe Type
 *
 * Un objet de type Type contient toutes les spécifications permettant de produire un document.
 *
 * Il fait référence au modéle de document au moyen d'un ContentType
 * Il spécifie les données à insérer au moyen d'un PartType.
 * Il indique également le type MIME du document à produire
 */
class FusionType
{
    public $template;
    public $mimeType;
    public $part;
    public $debug;

    /**
     * Constructeur
     *
     * @param object $template Le modele
     * @param string $mimeType Le type MIME du document à produire
     */
    public function __construct(ContentType $template, $mimeType, PartType $part)
    {

        $this->template = $template;
        $this->mimeType = $mimeType;
        $this->part = $part->finish();
    }

    public function setDebug()
    {
        $this->debug = true;
    }

    /**
     * L'array "classmap" établie la relation entre les types d'objets PHP
     * et les types utilisés dans le WSDL
     *
     * @return   array      La relation entre les types WSDL et les classes PHP
     */
    public function classmap()
    {
        return [
            'FieldType' => 'Libriciel\OfficeClients\Fusion\Type\FieldType',
            'ContentType' => 'Libriciel\OfficeClients\Fusion\Type\ContentType',
            'DrawingType' => 'Libriciel\OfficeClients\Fusion\Type\DrawingType',
            'FusionType' => 'Libriciel\OfficeClients\Fusion\Type\FusionType',
            'IterationType' => 'Libriciel\OfficeClients\Fusion\Type\IterationType',
            'PartType' => 'Libriciel\OfficeClients\Fusion\Type\PartType',
            'MatrixType' => 'Libriciel\OfficeClients\Fusion\Type\MatrixType',
            'MatrixRowType' => 'Libriciel\OfficeClients\Fusion\Type\MatrixRowType',
            'MatrixTitleType' => 'Libriciel\OfficeClients\Fusion\Type\MatrixTitleType',
        ];
    }
}

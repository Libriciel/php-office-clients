<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

/**
 * Classe MatrixRowType
 *
 * Un objet de type MatrixRowType contient un tableau de valeurs numériques.
 * Ces objets sont ensuite enregistrés dans un objet de type MatrixType.
 */
class MatrixRowType
{
    public $value;

    public function __construct($aValue)
    {
        $this->value = $aValue;
    }
}

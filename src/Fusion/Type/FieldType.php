<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

/**
 * Classe FieldType
 *
 * Un FieldType est un objet servant à alimenter la valeur d'un champ utilisateur dans le modèle de document.
 */
class FieldType
{
    public $target;
    public $value;
    public $dataType;

    /**
     * Constructeur
     *
     * @param string $name Non du champ utilisateur
     * @param string $value Valeur à insérer
     * @param string $sDataType type de donnée ("string", "number", "date", "text")
     */
    public function __construct($target, $value, $sDataType)
    {
        $this->target = $target;
        $this->value = preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $value); //Remove CTRL-CHAR
        if (in_array($sDataType, ['date', 'lines', 'number', 'text'], true)) {
            $this->dataType = $sDataType;
        } else {
            $this->dataType = "string";
        }
    }
}

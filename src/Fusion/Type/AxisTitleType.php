<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

/**
 * Classe MatrixTitleType
 *
 * Un objet de type MatrixTitleType contient optionnellement un titre qui sera affecté à l'un des axes et une série de
 * descriptions textuelles qui seront affichée pour chaque ligne ou chaque colonne de cet axe.
 */
class AxisTitleType
{
    public function setTitle($sTitle)
    {
        $this->title = $sTitle;
    }

    public function setDescription($aDescription)
    {
        $this->description = $aDescription;
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

/**
 * Classe MatrixType
 *
 * Un objet de type MatrixType permet de mettre à jour un diagramme ou toute représentation d'une matrice numérique.
 *
 * Outre les valeurs numériques organisées en un tableau de MatrixRowType, il permet de changer le libellé des axes
 * ainsi que les valeurs textuelles associées à chaque ligne ou à chaque colonne au moyen d'un objet de type
 * AxisTitleType.
 */
class MatrixType
{
    public $target;
    public $title;
    public $rowTitles;
    public $columnTitles;
    public $rowData = [];

    public function __construct($target)
    {

        $this->target = $target;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setRowTitles($rowTitle)
    {

        $this->rowTitles = $rowTitle;
    }

    public function setColumnTitles($columnTitle)
    {

        $this->columnTitles = $columnTitle;
    }

    public function addRow($matrixElement)
    {

        $this->rowData[] = $matrixElement;
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

use Exception;

/**
 * Classe ContentType
 *
 * Un objet de type ContentType contient les références à un document
 *
 * Ce document a un type MIME et peut se trouver
 *  - a une url donnée
 *  - dans l'objet lui-même sous forme binaire
 *  - dans l'objet lui-même sous forme d'un texte en html.
 */
class ContentType implements \JsonSerializable
{
    public $name;
    public $target;
    public $mimeType;
    public $url;
    public $binary;
    public $text;
    private $mode;

    /**
     * Constructeur
     *
     * @param string $name Non du document
     * @param string $mimeType type MIME du document
     * @param string $mode mode d'accés au contenu
     * @param string $value url, valeur binaire ou texte (html), selon le mode
     */
    public function __construct($target, $name, $mimeType, $mode, $value)
    {
        $this->mode = $mode;
        if ($target != '') {
            $this->target = $target;
        }
        if ($name != '') {
            $this->name = $name;
        }
        $this->mimeType = $mimeType;

        switch ($mode) {
            case 'url':
                $this->url = $value;
                break;
            case 'binary':
                $this->binary = $value;
                break;
            case 'text':
                $this->text = $value;
                break;
        }
    }

    /**
     * Renvoi le nom du document
     *
     * @return   string        Le nom du document
     */
    public function getName()
    {
        return ($this->name);
    }

    /**
     * Renvoi le type MIME du contenu
     *
     * @return   string        Le type MIME
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Renvoi le type MIME du contenu
     *
     * @return   string        Le type MIME
     * @throws Exception
     */
    public function getContent()
    {

        if (isset($this->url)) {
            return (file_get_contents($this->url));
        } elseif (isset($this->text)) {
            return ($this->text);
        } elseif (isset($this->binary)) {
            return ($this->binary);
        }

        throw new Exception('Content not available.');
    }

    /**
     * Renvoi le contenu vers le client.
     * Si le contenu est spécifié par une URL, il est d'abord récupéré.
     */
    public function sendToClient()
    {

        //  Accéder à toutes les données avant de lancer le premier header
        $sMimeType = $this->getMimeType();
        $sFileName = $this->getName();
        $bContent = $this->getContent();

        header('Content-type: ' . $sMimeType);
        header('Content-disposition: attachment; filename=' . $sFileName);
        header('Content-length: ' . strlen($bContent));

        echo $bContent;
    }

    /**
     * Renvoie le contenu vers le fichier spécifié.
     *
     * @param string $sFile Chemin du fichier où stocker le résultat
     */
    public function sendToFile($sFile)
    {
        file_put_contents($sFile, $this->getContent());
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize(): array
    {
        return [
            'name' => $this->name,
            'target' => $this->target,
            'mimeType' => $this->mimeType,
            'url' => $this->url,
            'binary' => base64_encode($this->binary),
            'text' => $this->text,
            'mode' => $this->mode
        ];
    }
}

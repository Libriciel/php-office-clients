<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

/**
 * Classe DrawingType
 *
 * Un objet de type Drawing permet de spécifier le nom d'un dessin Draw inséré dans le documents et la liste des figures
 * graphique dont il faut changer le style.
 */
class DrawingType
{
    public $name;
    public $shapes = [];

    /**
     * Constructeur
     *
     * @param string $name Non du dessin
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Ajoute un objet de type ShapeType
     *
     * @param object     l'objet ShapeType à ajouter à l'objet
     */
    public function addShape($aShape)
    {
        $this->shapes[] = $aShape;
    }
}

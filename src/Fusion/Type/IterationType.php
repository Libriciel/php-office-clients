<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Type;

/**
 * Classe IterationType
 *
 * Un objet de type Iteration permet de regrouper plusieurs parties identiques du document cible.
 * Chaque partie est un objet de type PartType.
 */
class IterationType
{
    public $name;
    public $part = [];

    /**
     * Constructeur
     *
     * @param string $name Non de l'itération
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Ajoute un objet de type PartType
     *
     * @param object     l'objet PartType à ajouter à l'itération
     */
    public function addPart($aPart)
    {

        $this->part[] = $aPart->finish();
    }
}

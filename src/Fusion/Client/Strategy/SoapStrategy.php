<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Client\Strategy;

use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use Libriciel\OfficeClients\Fusion\Client\Configuration\SoapServiceConfiguration;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Fusion\Exception\InvalidTemplateException;
use Libriciel\OfficeClients\Fusion\Type\ContentType;
use Libriciel\OfficeClients\Fusion\Type\FusionType;
use Libriciel\OfficeClients\Fusion\Type\PartType;
use SoapClient;
use SoapFault;

class SoapStrategy implements StrategyInterface
{
    protected SoapServiceConfiguration $config;

    public function __construct(SoapServiceConfiguration $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $templatePath
     * @param PartType $part
     * @return string
     * @throws \Libriciel\OfficeClients\Exception\ConnectionException
     * @throws \Libriciel\OfficeClients\Fusion\Exception\InvalidTemplateException
     */
    public function fusion(string $templatePath, PartType $part): string
    {
        try {
            $template = new ContentType(
                '',
                basename($templatePath),
                mime_content_type($templatePath),
                'binary',
                file_get_contents($templatePath)
            );

            $fusion = new FusionType($template, 'application/vnd.oasis.opendocument.text', $part);

            $options = [
                'cache_wsdl' => WSDL_CACHE_NONE,
                'classmap' => $fusion->classmap(),
                'exceptions' => 1,
                'trace' => 1
            ];

            if ($this->config->proxy !== null) {
                $options['proxy_host'] = $this->config->proxy->host;
                $options['proxy_port'] = $this->config->proxy->port;
                $options['proxy_login'] = $this->config->proxy->username;
                $options['proxy_password'] = $this->config->proxy->password;
            }

            $client = new SoapClient($this->config->baseUri, $options);
            $result = $client->{$this->config->pathFusion}($fusion);

            $headers = $client->__getLastResponseHeaders();
            $status = 0;
            if (preg_match('/HTTP\/[0-9].[0-9] ([0-9]+)/', $headers, $matches)) {
                $status = (int)$matches[1];
            }

            if ($this->config->expectedHttpStatus !== null) {
                if ($status !== $this->config->expectedHttpStatus) {
                    $message = sprintf(
                        'Got HTTP status %d calling %s (expected %s)',
                        $status,
                        $this->config->baseUri,
                        $this->config->expectedHttpStatus
                    );
                    throw new UnexpectedHttpStatusException($message, $result->content->binary);
                }
            }

            return $result->content->binary;
        } catch (SoapFault $exc) {
            if (preg_match('/Incapable de lire le modèle/im', $exc->getMessage())) {
                throw new InvalidTemplateException(sprintf('Incapable de lire le modèle ODT "%s"', $templatePath));
            }
            throw new ConnectionException($exc->getMessage(), $exc->getCode(), $exc);
        }
    }

    /**
     * @return string
     * @throws \Libriciel\OfficeClients\Exception\ConnectionException
     */
    public function version(): string
    {
        try {
            $client = new SoapClient(
                $this->config->baseUri,
                ['cache_wsdl' => WSDL_CACHE_NONE, 'exceptions' => 1, 'trace' => 1]
            );
            $result = $client->__soapCall($this->config->pathVersion, []);

            $headers = $client->__getLastResponseHeaders();
            $status = 0;
            if (preg_match('/HTTP\/[0-9].[0-9] ([0-9]+)/', $headers, $matches)) {
                $status = (int)$matches[1];
            }

            if ($status !== 200) {
                $format = "Got HTTP status %d calling %s (expected 200)\n%s";
                $message = sprintf($format, $status, $this->config->baseUri, $result);
                throw new ConnectionException($message);
            }

            return $result;
        } catch (SoapFault $exc) {
            throw new ConnectionException($exc->getMessage(), $exc->getCode(), $exc);
        }
    }
}

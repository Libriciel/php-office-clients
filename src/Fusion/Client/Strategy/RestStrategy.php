<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Client\Strategy;

use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use Libriciel\OfficeClients\Fusion\Client\Configuration\RestServiceConfiguration;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Fusion\Exception\InvalidTemplateException;
use Libriciel\OfficeClients\Fusion\Type\ContentType;
use Libriciel\OfficeClients\Fusion\Type\PartType;
use Libriciel\OfficeClients\Strategy\AbstractCurlStrategy;
use stdClass;

class RestStrategy extends AbstractCurlStrategy implements StrategyInterface
{
    protected RestServiceConfiguration $config;

    public function __construct(RestServiceConfiguration $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $templatePath
     * @param PartType $part
     * @return string
     * @throws \Libriciel\OfficeClients\Exception\ConnectionException
     * @throws \Libriciel\OfficeClients\Fusion\Exception\InvalidTemplateException
     */
    public function fusion(string $templatePath, PartType $part): string
    {
        $template = new ContentType(
            '',
            basename($templatePath),
            mime_content_type($templatePath),
            'binary',
            file_get_contents($templatePath)
        );

        $object = new stdClass();
        $object->template = $template;
        $object->mimeType = 'application/vnd.oasis.opendocument.text';
        $object->part = $part->finish();

        $data = json_encode((object)array_filter((array)$object));
        $url = $this->config->baseUri . $this->config->pathFusion;

        $handle = curl_init($url);

        $options = $this->getBaseCurlOptions();
        $options[CURLOPT_CUSTOMREQUEST] = 'POST';
        $options[CURLOPT_HTTPHEADER][] = 'Content-Type: application/json';
        $options[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen($data);
        $options[CURLOPT_POSTFIELDS] = $data;
        curl_setopt_array($handle, $options);

        $result = (string)curl_exec($handle);
        $status = (int)curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        if ($status === 0) {
            throw new ConnectionException(sprintf('Could not reach %s', $url));
        }

        if ($this->config->expectedHttpStatus !== null) {
            if ($status !== $this->config->expectedHttpStatus) {
                if (preg_match('/Incapable de lire le modèle/im', $result)) {
                    throw new InvalidTemplateException(sprintf('Incapable de lire le modèle ODT "%s"', $templatePath));
                }
                $message = sprintf(
                    'Got HTTP status %d calling %s (expected %s)',
                    $status,
                    $url,
                    $this->config->expectedHttpStatus
                );
                throw new UnexpectedHttpStatusException($message, $result);
            }
        }

        return $result;
    }

    /**
     * @return string
     * @throws \Libriciel\OfficeClients\Exception\ConnectionException
     */
    public function version(): string
    {
        $url = $this->config->baseUri . $this->config->pathVersion;

        $handle = curl_init();

        $options = $this->getBaseCurlOptions();
        $options[CURLOPT_URL] = $url;
        curl_setopt_array($handle, $options);

        $result = (string)curl_exec($handle);
        $status = (int)curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        if ($status === 0) {
            throw new ConnectionException(sprintf('Could not reach %s', $url));
        }

        if ($status !== 200) {
            $message = sprintf("Got HTTP status %d calling %s (expected 200)\n%s", $status, $url, $result);
            throw new ConnectionException($message);
        }

        return $result;
    }
}

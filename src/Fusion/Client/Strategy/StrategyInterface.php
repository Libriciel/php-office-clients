<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Client\Strategy;

use Libriciel\OfficeClients\Fusion\Type\PartType;

interface StrategyInterface
{
    public function fusion(string $templatePath, PartType $part): string;

    /**
     * Retourne la version du web-service
     *
     * @return string
     * @throws \Libriciel\OfficeClients\Exception\ConnectionException
     */
    public function version(): string;
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Client;

use Libriciel\OfficeClients\Fusion\Client\Configuration\ServiceConfigurationInterface;
use Libriciel\OfficeClients\Fusion\Client\Configuration\RestServiceConfiguration;
use Libriciel\OfficeClients\Fusion\Client\Configuration\SoapServiceConfiguration;
use Libriciel\OfficeClients\Exception\InvalidStrategyException;
use Libriciel\OfficeClients\Fusion\Client\Strategy\RestStrategy;
use Libriciel\OfficeClients\Fusion\Client\Strategy\StrategyInterface;
use Libriciel\OfficeClients\Fusion\Client\Strategy\SoapStrategy;

abstract class ClientFactory
{
    public const REST = 'rest';
    public const SOAP = 'soap';

    public static function create(
        ?string $strategy = null,
        ?ServiceConfigurationInterface $config = null
    ): StrategyInterface {
        $strategy = $strategy ?: (getenv('PHP_OFFICE_CLIENTS_FUSION_DEFAULT_STRATEGY') ?: static::REST);

        switch ($strategy) {
            case static::REST:
                return new RestStrategy($config ?: new RestServiceConfiguration());
            case static::SOAP:
                return new SoapStrategy($config ?: new SoapServiceConfiguration());
            default:
                $format = 'Invalid strategy "%s" requested, use one of "rest", "soap".';
                throw new InvalidStrategyException(sprintf($format, $strategy));
        }
    }
}

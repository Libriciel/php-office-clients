<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Client\Configuration;

use Libriciel\OfficeClients\Configuration\AbstractServiceConfiguration as BaseAbstractServiceConfiguration;
use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;

abstract class AbstractServiceConfiguration extends BaseAbstractServiceConfiguration implements ServiceConfigurationInterface // phpcs:ignore
{
    protected const ENV_PREFIX = 'PHP_OFFICE_CLIENTS_FUSION';

    public string $pathFusion;
    public string $pathVersion;

    public function __construct(
        ?string $baseUri = null,//@todo
        ?string $pathFusion = null,
        ?string $pathVersion = null,
        ?ServiceProxyConfiguration $proxy = null,
        ?bool $verbose = null,
        ?int $expectedHttpStatus = null
    ) {
        $prefix = static::ENV_PREFIX . '_DEFAULT_';

        $this->baseUri = $this->getString($baseUri, $prefix . 'BASE_URI', static::DEFAULT_BASE_URI);
        $this->pathFusion = $this->getString($pathFusion, $prefix . 'PATH_FUSION', static::DEFAULT_PATH_FUSION);
        $this->pathVersion = $this->getString($pathVersion, $prefix . 'PATH_VERSION', static::DEFAULT_PATH_VERSION);
        $this->proxy = $this->getProxy($proxy, $prefix . 'PROXY', null);
        $this->verbose = $this->getBool($verbose, $prefix . 'VERBOSE', static::DEFAULT_VERBOSE);
        $this->expectedHttpStatus = $this->getInt(
            $expectedHttpStatus,
            $prefix . 'EXPECTED_HTTP_STATUS',
            static::DEFAULT_EXPECTED_HTTP_STATUS
        );
    }
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Client\Configuration;

class RestServiceConfiguration extends AbstractServiceConfiguration
{
    protected const ENV_PREFIX = 'PHP_OFFICE_CLIENTS_FUSION_REST';

    protected const DEFAULT_BASE_URI = 'http://flow:8080';
    protected const DEFAULT_PATH_FUSION = '/ODFgedooo/rest/fusion';
    protected const DEFAULT_PATH_VERSION = '/ODFgedooo/rest/version';
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Client\Configuration;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;

interface ServiceConfigurationInterface
{
    public function __construct(
        ?string $baseUri = null,
        ?string $pathFusion = null,
        ?string $pathVersion = null,
        ?ServiceProxyConfiguration $proxy = null,
        ?bool $verbose = null,
        int $expectedHttpStatus = null
    );
}

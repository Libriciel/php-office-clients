<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Fusion\Client\Configuration;

class SoapServiceConfiguration extends AbstractServiceConfiguration
{
    protected const ENV_PREFIX = 'PHP_OFFICE_CLIENTS_FUSION_SOAP';

    protected const DEFAULT_BASE_URI = 'http://flow:8080';
    protected const DEFAULT_PATH_FUSION = 'Fusion';
    protected const DEFAULT_PATH_VERSION = 'Version';
}

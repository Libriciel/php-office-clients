<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Conversion\Client\Strategy;

interface StrategyInterface
{
    /**
     * File content initialization and converting from odt to pdf
     *
     * @param string $content File content to convert
     * @param string $inputFormat Input file format
     * @param string $outputFormat Output file format
     * @return string
     * @throws \Throwable
     */
    public function conversion(string $content, $inputFormat = 'odt', $outputFormat = 'pdf'): string;
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Conversion\Client\Strategy;

use Libriciel\OfficeClients\Conversion\Client\Configuration\CloudoooServiceConfiguration;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use PhpXmlRpc\Client;
use PhpXmlRpc\Request;
use PhpXmlRpc\Value;

class CloudoooStrategy implements StrategyInterface
{
    protected CloudoooServiceConfiguration $config;

    public function __construct(CloudoooServiceConfiguration $config)
    {
        $this->config = $config;
    }

    public function conversion(string $content, $inputFormat = 'odt', $outputFormat = 'pdf'): string
    {
        $params = new Request($this->config->path, [
            new Value(base64_encode($content), 'string'),
            new Value($inputFormat, 'string'),
            new Value($outputFormat, 'string'),
            new Value(false, 'boolean'),
            new Value(true, 'boolean')
        ]);

        $client = new Client($this->config->baseUri);
        $client->setUseCurl($this->config->useCurl);

        if ($this->config->proxy !== null) {
            $client->setProxy(
                $this->config->proxy->host,
                $this->config->proxy->port,
                $this->config->proxy->username,
                $this->config->proxy->password
            );
        }

        if ($this->config->verbose) {
            $params->debug = 1;
        }

        $response = $client->send($params);
        if (empty($response)) {
            $message = sprintf(
                "Got empty response calling %s:\n%s",
                $this->config->baseUri,
                $client->setDebug(3)
            );
            throw new ConnectionException($message);
        }

        $status = (int)($response->httpResponse()['status_code'] ?? null);

        if ($status === 0) {
            throw new ConnectionException(sprintf('Could not reach %s', $this->config->baseUri));
        }

        if ($this->config->expectedHttpStatus !== null && $status !== $this->config->expectedHttpStatus) {
            $message = sprintf(
                'Got HTTP status %d instead of expected HTTP status %d calling %s',
                $status,
                $this->config->expectedHttpStatus,
                $this->config->baseUri
            );
            throw new UnexpectedHttpStatusException($message, $response->value()->scalarVal());
        }

        if (empty($response->value())) {
            $message = sprintf(
                "Got empty response value calling %s:\n%s",
                $this->config->baseUri,
                $response->faultString()
            );
            throw new ConnectionException($message);
        }

        return base64_decode($response->value()->scalarval());
    }
}

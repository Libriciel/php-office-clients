<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Conversion\Client\Strategy;

use CURLFile;
use CURLStringFile;
use Libriciel\OfficeClients\Conversion\Client\Configuration\UnoserverServiceConfiguration;
use Libriciel\OfficeClients\Conversion\Exception\InvalidInputFormatException;
use Libriciel\OfficeClients\Exception\ConnectionException;
use Libriciel\OfficeClients\Exception\UnexpectedHttpStatusException;
use Libriciel\OfficeClients\Strategy\AbstractCurlStrategy;

class UnoserverStrategy extends AbstractCurlStrategy implements StrategyInterface
{
    protected UnoserverServiceConfiguration $config;

    public function __construct(UnoserverServiceConfiguration $config)
    {
        $this->config = $config;
    }

    public function conversion(string $content, $inputFormat = 'odt', $outputFormat = 'pdf'): string
    {
        if ($inputFormat === 'odt') {
            $basename = 'document.odt';
            $mimetype = 'application/vnd.oasis.opendocument.text';
        } else {
            $message = sprintf('Invalid input format "%s", use one of "odt"', $inputFormat);
            throw new InvalidInputFormatException($message);
        }

        $tempnam = null;

        try {
            if ($this->config->useStream) {
                // @see: https://php.watch/versions/8.1/CURLStringFile
                $file = new CURLStringFile($content, $mimetype, $basename);
            } else {
                $tempnam = tempnam(
                    sys_get_temp_dir(),
                    preg_replace('/^.*\\\\([^\\\\]+)::(.*)$/', '\1-\2_', __METHOD__)
                );
                file_put_contents($tempnam, $content);
                $file = new CURLFile($tempnam, $mimetype, $basename);
            }

            $url = $this->config->baseUri . $this->config->path;

            $handle = curl_init();

            $options = $this->getBaseCurlOptions();
            $options[CURLOPT_HTTPHEADER][] = 'Content-Type:multipart/form-data';
            $options[CURLOPT_POST] = 1;
            $options[CURLOPT_POSTFIELDS] = [
                'convert-to' => $outputFormat,
                'file' => $file,
            ];
            $options[CURLOPT_URL] = $url;
            curl_setopt_array($handle, $options);

            $result = (string)curl_exec($handle);
            $status = (int)curl_getinfo($handle, CURLINFO_HTTP_CODE);
            curl_close($handle);

            if ($status === 0) {
                throw new ConnectionException(sprintf('Could not reach %s', $url));
            }

            if ($this->config->expectedHttpStatus !== null && $status !== $this->config->expectedHttpStatus) {
                $message = sprintf(
                    "Got HTTP status %d instead of expected HTTP status %d calling %s",
                    $status,
                    $this->config->expectedHttpStatus,
                    $this->config->baseUri
                );
                throw new UnexpectedHttpStatusException($message, (string)$result);
            }
        } finally {
            if ($tempnam !== null) {
                unlink($tempnam);
            }
        }

        return $result;
    }
}

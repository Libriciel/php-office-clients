<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Conversion\Client\Configuration;

class UnoserverServiceConfiguration extends AbstractServiceConfiguration
{
    protected const ENV_PREFIX = 'PHP_OFFICE_CLIENTS_CONVERSION_UNOSERVER';

    protected const DEFAULT_BASE_URI = 'http://unoserver:2004';
    protected const DEFAULT_PATH = '/request';

    public bool $useStream = true;
}

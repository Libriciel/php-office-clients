<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Conversion\Client\Configuration;

class CloudoooServiceConfiguration extends AbstractServiceConfiguration
{
    protected const ENV_PREFIX = 'PHP_OFFICE_CLIENTS_CONVERSION_CLOUDOOO';

    protected const DEFAULT_BASE_URI = 'http://cloudooo:8011';
    protected const DEFAULT_PATH = 'convertFile';

    public int $useCurl = 2;
}

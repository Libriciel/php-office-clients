<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Conversion\Client\Configuration;

use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;

interface ServiceConfigurationInterface
{
    public function __construct(
        ?string $url = null,
        ?string $convert = null,
        ?ServiceProxyConfiguration $proxy = null,
        ?bool $verbose = null,
        int $expectedHttpStatus = null
    );
}

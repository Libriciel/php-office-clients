<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Conversion\Client\Configuration;

use Libriciel\OfficeClients\Configuration\AbstractServiceConfiguration as BaseAbstractServiceConfiguration;
use Libriciel\OfficeClients\Configuration\ServiceProxyConfiguration;

abstract class AbstractServiceConfiguration extends BaseAbstractServiceConfiguration implements ServiceConfigurationInterface // phpcs:ignore
{
    protected const ENV_PREFIX = 'PHP_OFFICE_CLIENTS_CONVERSION';

    public string $path;
}

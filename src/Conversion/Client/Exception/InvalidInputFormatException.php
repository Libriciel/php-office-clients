<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Conversion\Exception;

use RuntimeException;

class InvalidInputFormatException extends RuntimeException
{
}

<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Conversion\Client;

use Libriciel\OfficeClients\Conversion\Client\Configuration\ServiceConfigurationInterface;
use Libriciel\OfficeClients\Conversion\Client\Configuration\UnoserverServiceConfiguration;
use Libriciel\OfficeClients\Conversion\Client\Configuration\CloudoooServiceConfiguration;
use Libriciel\OfficeClients\Exception\InvalidStrategyException;
use Libriciel\OfficeClients\Conversion\Client\Strategy\CloudoooStrategy;
use Libriciel\OfficeClients\Conversion\Client\Strategy\StrategyInterface;
use Libriciel\OfficeClients\Conversion\Client\Strategy\UnoserverStrategy;

abstract class ClientFactory
{
    public const CLOUDOOO = 'cloudooo';
    public const UNOSERVER = 'unoserver';

    public static function create(
        ?string $strategy = null,
        ?ServiceConfigurationInterface $config = null
    ): StrategyInterface {
        $strategy = $strategy ?: (getenv('PHP_OFFICE_CLIENTS_CONVERSION_DEFAULT_STRATEGY') ?: static::UNOSERVER);
        $config = $config ?: (
            $strategy === static::CLOUDOOO
                ? new CloudoooServiceConfiguration()
                : new UnoserverServiceConfiguration()
        );

        switch ($strategy) {
            case static::CLOUDOOO:
                return new CloudoooStrategy($config);
            case static::UNOSERVER:
                return new UnoserverStrategy($config);
            default:
                $format = 'Invalid strategy "%s" requested, use one of "%s", "%s".';
                throw new InvalidStrategyException(sprintf($format, $strategy, static::CLOUDOOO, static::UNOSERVER));
        }
    }
}

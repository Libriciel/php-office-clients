<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Configuration;

class ServiceProxyConfiguration
{
    public ?string $host = null;
    public ?int $port = null;
    public ?string $username = null;
    public ?string $password = null;

    public function __construct(
        ?string $host = null,
        ?int $port = null,
        ?string $username = null,
        ?string $password = null
    ) {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
    }
}

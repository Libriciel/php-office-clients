<?php

declare(strict_types=1);

namespace Libriciel\OfficeClients\Configuration;

abstract class AbstractServiceConfiguration
{
    protected const DEFAULT_PROXY = false;
    protected const DEFAULT_PROXY_HOST = '';
    protected const DEFAULT_PROXY_PORT = null;
    protected const DEFAULT_PROXY_USERNAME = '';
    protected const DEFAULT_PROXY_PASSWORD = '';
    protected const DEFAULT_VERBOSE = false;
    protected const DEFAULT_EXPECTED_HTTP_STATUS = 200;

    public string $baseUri;
    public string $path;
    public ?ServiceProxyConfiguration $proxy;
    public bool $verbose;
    public int $expectedHttpStatus;

    protected function getBool(?bool $value, string $envKey, bool $default): bool
    {
        if ($value !== null) {
            return $value;
        }

        $value = getenv($envKey);
        if ($value !== false) {
            if ($value === '0') {
                return false;
            } elseif ($value === '1') {
                return true;
            }
        }

        return $default;
    }

    protected function getInt(?int $value, string $envKey, ?int $default): ?int
    {
        if ($value !== null) {
            return $value;
        }

        $value = getenv($envKey);
        if ($value !== false) {
            return (int)$value;
        }

        return $default;
    }

    protected function getProxy(
        ?ServiceProxyConfiguration $value,
        string $envKeySuffix,
        ?ServiceProxyConfiguration $default
    ): ?ServiceProxyConfiguration {
        if ($value !== null) {
            return $value;
        }

        if ($this->getBool(null, $envKeySuffix, static::DEFAULT_PROXY)) {
            $prefix = $envKeySuffix . '_';
            return new ServiceProxyConfiguration(
                $this->getString(null, $prefix . 'HOST', static::DEFAULT_PROXY_HOST),
                $this->getInt(null, $prefix . 'PORT', static::DEFAULT_PROXY_PORT),
                $this->getString(null, $prefix . 'USERNAME', static::DEFAULT_PROXY_USERNAME),
                $this->getString(null, $prefix . 'PASSWORD', static::DEFAULT_PROXY_PASSWORD)
            );
        }

        return $default;
    }

    protected function getString(?string $value, string $envKey, string $default): string
    {
        if ($value !== null) {
            return $value;
        }

        $value = getenv($envKey);
        if ($value !== false) {
            return (string)$value;
        }

        return $default;
    }

    public function __construct(
        ?string $baseUri = null,
        ?string $path = null,
        ?ServiceProxyConfiguration $proxy = null,
        ?bool $verbose = null,
        ?int $expectedHttpStatus = null
    ) {
        $prefix = static::ENV_PREFIX . '_DEFAULT_';

        $this->baseUri = $this->getString($baseUri, $prefix . 'BASE_URI', static::DEFAULT_BASE_URI);
        $this->path = $this->getString($path, $prefix . 'PATH', static::DEFAULT_PATH);
        $this->proxy = $this->getProxy($proxy, $prefix . 'PROXY', null);
        $this->verbose = $this->getBool($verbose, $prefix . 'VERBOSE', static::DEFAULT_VERBOSE);
        $this->expectedHttpStatus = $this->getInt(
            $expectedHttpStatus,
            $prefix . 'EXPECTED_HTTP_STATUS',
            static::DEFAULT_EXPECTED_HTTP_STATUS
        );
    }
}

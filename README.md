[![Version PHP](http://img.shields.io/badge/php-%207.4-8892BF.svg)](https://php.net/)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![pipeline](https://gitlab.libriciel.fr/libriciel/direction-du-developpement/bibliotheque/php-office-clients/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/libriciel/direction-du-developpement/bibliotheque/php-office-clients/commits/master)
[![coverage](https://gitlab.libriciel.fr/libriciel/direction-du-developpement/bibliotheque/php-office-clients/badges/master/coverage.svg)](https://gitlab.libriciel.fr/libriciel/direction-du-developpement/bibliotheque/php-office-clients/commits/master)

# php-office-clients

Clients PHP aux interfaces unifiées pour les services liés à la fusion documentaire, sans dépendance de _framework_.

- [_lspdf2odt_](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/golem/lspdf2odt), transformation de PDF en ODT
- [_flow_](https://gitlab.libriciel.fr/libriciel/direction-du-developpement/bibliotheque/flow) ou [_gedooo_ / _golem_](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/golem/golem/), fusion de variables dans un modèle ODT
- [_cloudooo_](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/golem/cloudooo) ou [_unoserver_](https://github.com/unoconv/unoserver), transformation de format de document au moyen de _LibreOffice_ 

Les configurations par défaut se font au moyen de variables d'environnement.

Testé avec

- __pdf2odt__
  - [`registry.libriciel.fr/public/actes/lspdf2odt:0.9.5`](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/golem/lspdf2odt)
  - [`gitlab.libriciel.fr:4567/libriciel/direction-du-developpement/bibliotheque/outils-documentaires/vectorstodoc:latest`](https://gitlab.libriciel.fr/libriciel/direction-du-developpement/bibliotheque/outils-documentaires/vectorstodoc)
- __fusion__
  - [`registry.libriciel.fr/public/actes/golem:2.1.8`](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/golem/golem)
  - [`registry.libriciel.fr/public/libriciel/outils-documentaires/flow:1.0.0-rc.6`](https://gitlab.libriciel.fr/libriciel/direction-du-developpement/bibliotheque/outils-documentaires/flow)
- __conversion__
  - [`registry.libriciel.fr/public/actes/cloudooo:lo-7.0`](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/golem/cloudooo)
  - [`libreofficedocker/libreoffice-unoserver:3.18`](https://github.com/libreofficedocker#pre-built-image)

## Anciennes librairies

- [cakephp-fusionconv](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/webACTES/cakephp-fusionconv)
- [phpgedooo](https://gitlab.libriciel.fr/libriciel/pole-actes-administratifs/golem/phpgedooo)


## Utilisation

### Pour aller plus loin

- [`doc/pdf2odt-usage.md`](doc/pdf2odt-usage.md)
- [`doc/fusion-usage.md`](doc/fusion-usage.md)
- [`doc/fusion-migration-guide.md`](doc/fusion-migration-guide.md)
- [`doc/conversionn-usage.md`](doc/conversion-usage.md)

### Exemples

#### Pdf2Odt

```php
use Libriciel\OfficeFusion\Pdf2Odt\Client\ClientFactory;

try {
    $documentContent = ClientFactory::create()
        ->pdf2odt('/data/workspace/document.pdf');
    // ...
} catch(\Throwable $exc) {
    // ...
}
```

#### Fusion

```php
use Libriciel\OfficeFusion\Fusion\Client\ClientFactory;
use Libriciel\OfficeFusion\Fusion\Helper\Builder;

try {
    // Création des données
    $main = (new Builder())
        ->addField('entity_name', 'Libriciel SCOP')
        // ...
    ;

    $documentContent = ClientFactory::create()
        ->fusion('/data/workspace/template.odt', $main->getResult());
    // ...
} catch(\Throwable $exc) {
    // ...
}
```

#### Conversion

```php
use Libriciel\OfficeFusion\Conversion\Client\ClientFactory;

try {
    $documentContent = ClientFactory::create()
        ->conversion(file_get_contents('/data/workspace/document.odt'), 'odt', 'pdf');
    // ...
} catch(\Throwable $exc) {
    // ...
}
```

## Développement

```bash
docker compose -f docker-compose-dev.yml up -d --build
docker compose -f docker-compose-dev.yml logs -f
docker compose -f docker-compose-dev.yml exec php-office-clients /bin/bash
docker compose -f docker-compose-dev.yml down --remove-orphans --volumes
```

```bash
composer pre-commit
```

### Tests d'intégration incomplets

- `flow` devrait envoyer des `410 Gone` (à partir de la `1.0.0-rc.4`) pour l'URL du _WSDL_ (les tests sont _skipped_ actuellement)

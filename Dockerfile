FROM ubuntu:20.04

SHELL [ \
    "/bin/bash", \
    "-o", "errexit", \
    "-o", "nounset", \
    "-o", "pipefail", \
    "-c" \
]

ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_VERSION=2.2.21
ENV DEBIAN_FRONTEND=noninteractive
ENV DIR_PROJECT=/var/www/php-office-clients
ENV HTTP_USER=www-data
ENV HTTP_GROUP=www-data

RUN apt-get update \
        --assume-yes \
        --quiet=2 \
    && apt-get install \
        --assume-yes \
        --no-install-recommends \
        --quiet=2 \
        git=1:2.25.* \
        php7.4-cli=7.4.* \
        php7.4-curl=7.4.* \
        php7.4-mbstring=7.4.* \
        php7.4-soap=7.4.* \
        php7.4-xml=7.4.* \
        php7.4-zip=7.4.* \
        php-pcov=1.0.* \
        unzip=6.0-* \
        wget=1.20.* \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && wget --no-verbose --no-check-certificate --quiet https://getcomposer.org/installer --output-document=/tmp/composer-installer.php \
    && php /tmp/composer-installer.php --filename composer --install-dir=/usr/local/bin --no-ansi --version ${COMPOSER_VERSION}

RUN mkdir -p "${DIR_PROJECT}"

COPY --chown=${HTTP_USER}:${HTTP_GROUP} . "${DIR_PROJECT}"

WORKDIR "${DIR_PROJECT}"

RUN composer install

ENTRYPOINT [ "sleep", "infinity" ]

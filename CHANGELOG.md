# _Changelog_

## 1.9.0

- suppression du support SOAP


## 1.0.0

- Modernisation du code (PHP 7.4+)
- Ajout de l'intégration continue
- Ajout de tests d'intégration avec _cloudooo_, _gedooo_, _flow_ et _unoserver_
